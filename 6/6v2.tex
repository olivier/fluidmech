\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://framagit.org/olivier/sensible-styles

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger preamble)
\printversion
\usepackage{soul} %for strikethrough text with \st

\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%ù
\sectionstar{Balance of linear momentum}

		
		\pictureframe{1}{isaac3}{}{}
		
		{\setbeamercolor{background canvas}{bg=black}
			\begin{frame}{}
			\large
				\begin{center}
				\color{white}
					\textsc{May the Force be}
					
					\includegraphics[width=5cm]{isaac2}\pause
					
					\textsc{equal to the Mass times Acceleration}
				\end{center}
			
			\end{frame}
		}


	\subsectionstar{The Cauchy equation}


		\figureframe{0.5}{particle_sum_forces_0}{}{\wcfile{Forces and mass flow infinitesimal volume element.svg}{Figure} \cczero \oc}
		\figureframe{1}{particle_sum_forces}{}{\wcfile{Forces and mass flow infinitesimal volume element.svg}{Figure} \cczero \oc}

		\begin{frame}{Newton from two points of view:}

			\begin{IEEEeqnarray*}{rCl}\pause
				m_\text{part.} \derivative{\vec V}{t} & = & \vec F_\text{weight} + \vec F_\text{net, pressure} + \vec F_\text{net, shear}\nonumber\\
			\end{IEEEeqnarray*}
			\begin{IEEEeqnarray*}{rCl}\pause
				\rho \totaltimederivative{\vec V} & = & \rho \vec g + \frac{1}{\diff \vol} \vec F_\text{net, pressure} + \frac{1}{\diff \vol} \vec F_\text{net, shear}\nonumber\\ \label{eq:cauchytmp}
			\end{IEEEeqnarray*}\pause

			\small$\to$ How can we re-express pressure and shear?
		\end{frame}

		\begin{frame}{Pressure force:}
		
			Back from previous chapters:
			
			\setcounter{equation}{17}
			\begin{IEEEeqnarray*}{rCl}\pause
				\frac{1}{\diff \vol} \vec F_\text{net, pressure} & = & -\gradient{p} \label{eq:forcesdepression}
			\end{IEEEeqnarray*}
		\end{frame}
		
		\begin{frame}{Eulerian view of Newton}
			
			\begin{IEEEeqnarray*}{rCl}
				\rho \totaltimederivative{\vec V} & = & \rho \vec g + \textcolor{nicegreen}{\frac{1}{\diff \vol} \vec F_\text{net, pressure}} + \frac{1}{\diff \vol} \vec F_\text{net, shear}
			\end{IEEEeqnarray*}

			\small$\to$ OK for pressure, what about shear?
		\end{frame}

		\figureframe{0.7}{particle_shear_tensor_0}{}{\wcfile{Shear stress infinitesimal volume element.svg}{Figure} \cczero \oc}

		\begin{frame}{Shear in the $x$-direction:}
			\begin{centering}
			\includegraphics[width=7cm]{particle_shear_tensor_0}
			
			\end{centering}\vspace{-1cm}
			\begin{IEEEeqnarray*}{rCl}
				\vec F_{\text{shear}\ x} 	&=& \diff \vol \left(\partialderivative{\vec \tau_{zx}}{z} + \partialderivative{\vec \tau_{yx}}{y} + \partialderivative{\vec \tau_{xx}}{x}\right)\nonumber\\\pause
											&=& \diff \vol \ \divergent{\vec \tau_{ix}}\label{eq_fshear_xdir_divergent_two}
			\end{IEEEeqnarray*}
		\end{frame}
		
		\begin{frame}{Good memories from previous chapters}
			\begin{centering}
			\includegraphics[width=3cm]{particle_shear_tensor_0}
			
			\end{centering}\vspace{-1cm}
			\begin{IEEEeqnarray*}{rCl}
				\vec F_{\text{shear}} 	&=& \diff \vol \left(\begin{array}{c}%
												 \divergent{\vec \tau_{ix}}\\
												 \divergent{\vec \tau_{iy}}\\
												 \divergent{\vec \tau_{iz}}
													\end{array} \right)\\\pause
										&=& \diff \vol ~ \divergent{\vec \tau_{ij}}
			\end{IEEEeqnarray*}
		\end{frame}



		\begin{frame}{Aha!}
			We went from
				\begin{IEEEeqnarray*}{rCl}\pause
					\rho \totaltimederivative{\vec V} & = & \rho \vec g + \frac{1}{\vol} \vec F_\text{net, pressure} + \frac{1}{\vol} \vec F_\text{net, shear}
				\end{IEEEeqnarray*}\pause
			to\pause
			\begin{IEEEeqnarray*}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \divergent{\vec \tau_{ij}}
			\end{IEEEeqnarray*}\pause

			This is the \vocab{Cauchy equation}.
		\end{frame}


		\begin{frame}{the Cauchy equation}

			\begin{IEEEeqnarray*}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \divergent{\vec \tau_{ij}} \label{eq:cauchy}
			\end{IEEEeqnarray*}

			\small Regulates \textit{all fluid flows}, all the time!
		\end{frame}
		
		
		\begin{frame}{Now…}
			Can we eliminate $\divergent{\vec \tau_{ij}}$ from this equation?
			
			{\small \textit{(how can we express the divergent of the shear tensor $\vec \tau_{ij}$ as a function of the other fluid properties?)}}\pause
			
			$\to$ Enter Navier and Stokes.
			\begin{center}
			\includegraphics[height=3cm]{public_domain_images/Claude-Louis_Navier.jpg}\hspace{2cm}\includegraphics[height=3cm]{public_domain_images/Ggstokes.jpg}
			\end{center}
		\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\subsectionstarwithsubtitle{The Navier-Stokes equation for incompressible flow}{ba-dunn ba-dunn}
	
		\begin{frame}
			\begin{center}\pause
				Cauchy \pause
				
				=
				
				Newton’s 2\up{nd} law on a field
			\end{center}
		\end{frame}
	
		\begin{frame}
			\begin{center}
				Incompressible Navier-Stokes\pause
				
				=\pause
				
				Cauchy\\\pause
				+\\
				Newtonian fluid\\\pause
				+\\
				incompressible flow
			\end{center}
		\end{frame}
		
		\begin{frame}{Newtonian fluid?}\pause
		
			Then, the shear towards $j$ on a surface perpendicular to $i$ is
				\begin{IEEEeqnarray*}{rCcCl}
					||\vec \tau_{ij}|| 	&=& \mu \partialderivative{u_j}{i} \label{eq_shear_velocity_gradient_two}
				\end{IEEEeqnarray*}\pause
		
			\small$\to$ take the velocity in your direction,\\\pause
			~~~~~find its change perpendicular to the plane.
		
		\end{frame}
		
		\begin{frame}{Shear in the $x$-direction:}
			\begin{centering}
			\includegraphics[width=5cm]{particle_shear_tensor_0}
			
			\end{centering}\vspace{-1cm}\pause
			\begin{align*}
						\vec \tau_{xx 1} &= \mu \left.\partialderivative{u}{x}\right|_1 \vec i &
						\vec \tau_{xx 4} &= -\mu \left.\partialderivative{u}{x}\right|_4 \vec i\\
						\vec \tau_{yx 2} &= \mu \left.\partialderivative{u}{y}\right|_2 \vec i &
						\vec \tau_{yx 5} &= -\mu \left.\partialderivative{u}{y}\right|_5\vec i\\
						\vec \tau_{zx 3} &= \mu \left.\partialderivative{u}{z}\right|_3 \vec i&
						\vec \tau_{zx 6} &= -\mu \left.\partialderivative{u}{z}\right|_6 \vec i
			\end{align*}
		\end{frame}
		
		\begin{frame}{Shear in the $x$-direction:}\pause
			The net effect, if flow incompressible:\pause
			\begin{IEEEeqnarray*}{rCll}
				\divergent{\vec \tau_{ix}} \pause	&=& \partialderivative{\vec \tau_{xx}}{x} + \partialderivative{\vec \tau_{yx}}{y} + \partialderivative{\vec \tau_{zx}}{z}\nonumber\\\pause
					&=& \partialderivative{\left(\mu \partialderivative{u}{x} \vec i\right)}{x} + \partialderivative{\left(\mu \partialderivative{u}{y} \vec i\right)}{y} + \partialderivative{\left(\mu \partialderivative{u}{z} \vec i\right)}{z}\nonumber\\\pause
					&=& \mu \left(\secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} + \secondpartialderivative{u}{z}\right) \vec i\nonumber\\\label{eq_tmp_shear_der}
			\end{IEEEeqnarray*}
			
			\small \textit{there has to be a better way to write this!}
		\end{frame}
		
		\begin{frame}{the \vocab{Laplacian}}
			One more impress-your-date tool:
			\begin{IEEEeqnarray*}{rClll}
			 \laplacian{} &\equiv& \divergent{\gradient{}}\label{eq_def_laplacian}\\
			 \laplacian{A} &\equiv& \divergent{\gradient{A}}
			\end{IEEEeqnarray*}
		\end{frame}
		
		\begin{frame}{the \vocab{Laplacian}}
			One more impress-your-date tool:
			\begin{IEEEeqnarray*}{rClll}
				 \laplacian{\vec A} &\equiv& %
						\left(\begin{array}{c}%
							\laplacian{A_x}\\
							\laplacian{A_y}\\
							\laplacian{A_z}\\
					\end{array}\right) \ = \
						\left(\begin{array}{c}%
							\divergent{\gradient{A_x}}\\
							\divergent{\gradient{A_y}}\\
							\divergent{\gradient{A_z}}\\
					\end{array}\right)\nonumber\\
			\end{IEEEeqnarray*}
		\end{frame}
		
		\begin{frame}{Shear in the $x$-direction:}
			and so now:\pause
			\begin{IEEEeqnarray*}{rCll}
				\divergent{\vec \tau_{ix}} 	&=& \mu \left(\secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} + \secondpartialderivative{u}{z}\right) \vec i\nonumber\\\pause
				&=& \mu \laplacian{u} \ \vec i\\\pause
				&=& \mu \laplacian{\vec u}
			\end{IEEEeqnarray*}\pause
		
		\small Net shear effect is change in space \\of change in space of velocity\pause
		
		We made it!
		
		\end{frame}
		
		\begin{frame}{Three components of shear force}
			\begin{IEEEeqnarray*}{rCl}
				\divergent{\vec \tau_{ix}} &=& \mu \laplacian{\vec u}\\\pause
				\divergent{\vec \tau_{iy}} &=& \mu \laplacian{\vec v}\\\pause
				\divergent{\vec \tau_{iz}} &=& \mu \laplacian{\vec w}
			\end{IEEEeqnarray*}
		
		\end{frame}
		
		\begin{frame}{Three components of shear force}
			\begin{IEEEeqnarray*}{rCcCl}
				\divergent{\vec \tau_{ij}} &=& \pause\left(\begin{array}{c}%
						\divergent{\vec \tau_{ix}}\\
						\divergent{\vec \tau_{iy}}\\
						\divergent{\vec \tau_{iz}}
					\end{array}\right)\pause%
					&=& \left(\begin{array}{l}%
							\mu \laplacian{\vec u}\\
							\mu \laplacian{\vec v}\\
							\mu \laplacian{\vec w}
						\end{array}\right)\nonumber\\\pause
				\divergent{\vec \tau_{ij}} &=& \mu \laplacian{\vec V}
			\end{IEEEeqnarray*}\pause
		
		\small…which leads us to the glorious…
		
		\end{frame}
		
		
		\begin{frame}{The \vocab{incompressible\\ Navier-Stokes equation}}\pause
			\begin{centering}
			{\small acceleration = gravity + pressure + shear}

			\setcounter{equation}{39}
			\begin{mdframed}
			\begin{IEEEeqnarray*}{rCl}\pause
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}\nonumber\\\label{eq:navierstokes}
			\end{IEEEeqnarray*}
			\end{mdframed}

			Describes all ordinary flow!
			
			\end{centering}
		\end{frame}
		
		
		\begin{frame}{Question}
		\begin{centering}\pause
		
			If\pause
			\begin{IEEEeqnarray*}{rCl}
												0  & = &	\divergent{\vec V}\\\\
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}
			\end{IEEEeqnarray*}\pause
			
			then what is $\vec V$~?

		\end{centering}
		\end{frame}

	\skipinprint{
		\begin{frame}{}

			If you find the solution:\pause
			\begin{itemize}
				\item Win an \textsc{isut/lss} mug\pause
				\item Receive Nobel prize, Fields medal, Millennium prize
			\end{itemize}

		\end{frame}

		\begin{frame}{}
		
			Consequences:\pause
			\begin{itemize}
				\item \textsc{Ansys}, Autodesk, Siemens Automation etc.\ instantly collapse\pause
				\item Wind tunnels, water channels sold for scrap\pause
				\item Academics busy re-visiting everything they know\pause
				\item We have to buy a new \textsc{isut/lss} mug
			\end{itemize}

		\end{frame}
	}
		
		
		\begin{frame}{}
		
			we want \emph{the} solution, not \emph{a} solution
			
		\end{frame}
		
		\begin{frame}{}
		
			What do we mean by “the” solution?
			
			\includegraphics[width=4cm]{cannon}
			
		\end{frame}
		
		\figureframe{1}{cannon_trajectories}{What is the trajectory of the ball?}{Figure \cczero \oc}
		
		\begin{frame}{}\pause
			What is the solution to:
			\begin{IEEEeqnarray*}{rCl}
				\rho \timederivative{\vec V}  & = &	\rho \vec g
			\end{IEEEeqnarray*}\pause
		
			“\st{it depends}”\\\pause
			\emph{all solutions are:}\pause

			\begin{IEEEeqnarray*}{rCl}
				\vec V &=& \left(\begin{array}{l}%
							u_0 \\
							v_0 + g t
						\end{array}\right)
			\end{IEEEeqnarray*}
		\end{frame}
			
		
		\begin{frame}{Question}
		\begin{centering}
		
			If\pause
			\begin{IEEEeqnarray*}{rCl}
												0  & = &	\divergent{\vec V}\\\\
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}
			\end{IEEEeqnarray*}\pause
			
			then what is $\vec V$~?

		\end{centering}
		\end{frame}
		
		\oldpictureframe{}{Laminar_and_turbulent_flows_at_the_Noisiel_dam.jpg}{1}{\wcfile{Laminar_and_turbulent_flows_at_the_Noisiel_dam}{photo} \pd by \wcu{Tangopaso}}
		\oldpictureframe{}{illu2}{1}{photo \ccbysa \oc}
		\oldpictureframe{}{illu1}{1}{photo \ccbysa \oc}
		\oldpictureframe{}{illu3}{1}{\wcfile{Griffon Vulture 4.jpg}{photo} \ccbysa by \wcun{Juan_lacruz}{Juan Lacruz}}
		\oldpictureframe{}{F_A-18_Hornet_Solo_Display_(7278737178).jpg}{1}{\wcfile{F A-18 Hornet Solo Display (7278737178).jpg}{photo} \ccby by \flickrname{39463459@N08}{Peter Gronemann}}
		\oldpictureframe{}{USMC-04675.jpg}{1}{\wcfile{USMC-04675.jpg}{photo} by unknown photographer / USA Dept. of Defence (\pd)}

		\begin{frame}{The \vocab{incompressible\\ Navier-Stokes equation}}
		
				\begin{IEEEeqnarray*}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}
			\end{IEEEeqnarray*}

		\end{frame}
	
		\begin{frame}{}
		
			In Cartesian coordinates:
			\scriptsize
			\begin{IEEEeqnarray*}{cCc}\pause
				\rho \left[ \partialtimederivative{u} + u \partialderivative{u}{x} + v \partialderivative{u}{y} + w \partialderivative{u}{z} \right]  & = &	\rho g_x - \partialderivative{p}{x} + \mu \left[ \secondpartialderivative{u}{x} + \secondpartialderivative{u}{y} + \secondpartialderivative{u}{z} \right] \nonumber \\
				\\
				\nonumber\\\pause
				\rho \left[ \partialtimederivative{v} + u \partialderivative{v}{x} + v \partialderivative{v}{y} + w \partialderivative{v}{z} \right]  & = &	\rho g_y - \partialderivative{p}{y} + \mu \left[ \secondpartialderivative{v}{x} + \secondpartialderivative{v}{y} + \secondpartialderivative{v}{z} \right] \nonumber \\
				\\
				\nonumber\\\pause
				\rho \left[ \partialtimederivative{w} + u \partialderivative{w}{x} + v \partialderivative{w}{y} + w \partialderivative{w}{z} \right]  & = &	\rho g_z - \partialderivative{p}{z} + \mu \left[ \secondpartialderivative{w}{x} + \secondpartialderivative{w}{y} + \secondpartialderivative{w}{z} \right] \nonumber \\
			\end{IEEEeqnarray*}
		\end{frame}
		
		
		\begin{frame}{}
		Cartesian not working for you?\pause
		\scriptsize
			\begin{IEEEeqnarray*}{lr}
						\rho \left[ \partialtimederivative{v_r} + v_r \partialderivative{v_r}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} - \frac{v_\theta^2}{r} + v_z \partialderivative{v_r}{z} \right]  &\nonumber\\
						 \ \ \ \ \ = \	\rho g_r - \partialderivative{p}{r} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_r}{r} \right) - \frac{v_r}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_r}{\theta} -\frac{2}{r^2} \partialderivative{v_\theta}{\theta} + \secondpartialderivative{v_r}{z} \right] \nonumber \\
						\\
						\rho \left[ \partialtimederivative{v_\theta} + v_r \partialderivative{v_\theta}{r} + \frac{v_\theta}{r} \partialderivative{v_\theta}{\theta} + \frac{v_r v_\theta}{r} + v_z \partialderivative{v_\theta}{z} \right]  &\nonumber\\
						 \ \ \ \ \ = \	\rho g_\theta - \frac{1}{r} \partialderivative{p}{\theta} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_\theta}{r} \right) - \frac{v_\theta}{r^2} + \frac{1}{r^2} \secondpartialderivative{v_\theta}{\theta} + \frac{2}{r^2} \partialderivative{v_r}{\theta} + \secondpartialderivative{v_\theta}{z} \right] \nonumber \\
						\\
						\rho \left[ \partialtimederivative{v_z} + v_r \partialderivative{v_z}{r} + \frac{v_\theta}{r} \partialderivative{v_r}{\theta} + v_z \partialderivative{v_z}{z} \right]  &\nonumber\\
						 \ \ \ \ \ = \	\rho g_z - \partialderivative{p}{z} + \mu \left[ \frac{1}{r} \partialderivative{}{r} \left(r \partialderivative{v_z}{r} \right) + \frac{1}{r^2} \secondpartialderivative{v_z}{\theta} + \secondpartialderivative{v_z}{z} \right] \nonumber \\
				\end{IEEEeqnarray*}
		\end{frame}
	
		\begin{frame}{The \vocab{incompressible\\ Navier-Stokes equation}}
		
			\begin{IEEEeqnarray*}{rCl}
				\rho \totaltimederivative{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}
			\end{IEEEeqnarray*}\pause

			\begin{centering}
			
			\textit{it’s not the Bernoulli equation}

			\end{centering}

		\end{frame}
	
		\begin{frame}{What then?}
		
			Navier-Stokes helps us:
			
			\begin{itemize}\pause
				\item understand and quantify the influence of forces;\pause
				\item find analytical solutions sometimes (!)\pause
				\item \emph{find numerical solutions}.
			\end{itemize}
		
		\end{frame}

\end{document}
