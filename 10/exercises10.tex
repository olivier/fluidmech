\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{07}
   \renewcommand{\lasteditday}{09}
\renewcommand{\numberofthischapter}{10}
\renewcommand{\titleofthischapter}{\namechapterten}
\atstartofexercises

\fluidmechexercisestitle

\mecafluexboxen

\begin{boiboiboite}
In boundary layer flow, we assume that transition occurs at $\rex \gtrsim \num{5e5}$.

The wall shear coefficient $c_f$, a function of distance $x$,\\
is defined based on the free-stream flow velocity~$U$:
		\begin{IEEEeqnarray}{rCl}
			c_{f_{(x)}} &\equiv& \frac{\tau_\text{wall}}{\frac{1}{2} \rho U^2} \ztag{\ref{eq_def_shear_coeff}}
		\end{IEEEeqnarray}

Exact solutions to the laminar boundary layer along a smooth surface yield:
	\begin{align}
			\frac{\delta}{x}		&=	\frac{\num{4,91}}{\sqrt{\rex}}
			&\frac{\delta^*}{x}		&=	\frac{\num{1,72}}{\sqrt{\rex}} \tag{\ref{eq_deltastar_lam}}\\
			\frac{\delta^{**}}{x}		&=	\frac{\num{0,664}}{\sqrt{\rex}}
			&c_{f_{(x)}}			&=	\frac{\num{0,664}}{\sqrt{\rex}} \tag{\ref{eq_cf_lam}}
	\end{align}

Solutions to the turbulent boundary layer along a smooth surface yield the following time-averaged characteristics:
	\begin{align}
			\frac{\delta}{x}		&\approx	\frac{\num{0,16}}{\rex^{\frac{1}{7}}}
			&\frac{\delta^*}{x}		&\approx	\frac{\num{0,02}}{\rex^{\frac{1}{7}}} \tag{\ref{eq_deltastar_turb}}\\
			\frac{\delta^{**}}{x}		&\approx	\frac{\num{0,016}}{\rex^{\frac{1}{7}}}
			&c_{f_{(x)}}			&\approx	\frac{\num{0,027}}{\rex^{\frac{1}{7}}} \tag{\ref{eq_cf_turb}}
	\end{align}
	\Cref{fig_viscosity_values_ex4} quantifies the viscosity of various fluids as a function of temperature.
\end{boiboiboite}

\begin{figure}
	\begin{center}
		\includegraphics[width=\textwidth]{viscosity_small.pdf}
	\end{center}
	\supercaption{The viscosity of four fluids (crude oil, water, air, and $\mathrm{CO_2}$) as a function of temperature. The scale for liquids is logarithmic and displayed on the left; the scale for gases is linear and displayed on the right.\vspace{-0.2cm}}{Figure reproduced from figure~\ref{fig_viscosity_values} p.~\pageref{fig_viscosity_values}; \ccby by Arjun Neyyathala \& \olivier}
	\label{fig_viscosity_values_ex4}
\end{figure}


%%%%
\subsubsection{Water and air flow}
\wherefrom{White \smallcite{white2008} E7.2}
\label{exo_water_air_flow}

	A flat plate of length~\SI{0,3}{\metre} is placed parallel to a uniform flow with speed \SI{0,3}{\metre\per\second}. How thick can the boundary layer become:
	\begin{enumerate}
		\item if the fluid is air at \SI{1}{\bar} and \SI{20}{\degreeCelsius}?
		\item if the fluid is water at \SI{20}{\degreeCelsius}?
	\end{enumerate}


%%%%
\subsubsection{Boundary layer sketches}
\wherefrom{\cczero \oc}
\label{exo_bl_sketches}

	A thin and long horizontal plate is moved horizontally through a stationary fluid.
	\begin{enumerate}
		\item Sketch the velocity profile of the fluid:
			\begin{itemize}
				\item at the leading edge;
				\item at a point where the boundary layer is laminar;
				\item and at a point further downstream where the boundary layer is turbulent.
			\end{itemize}
		\item Draw a few streamlines, indicate the boundary layer thickness~$\delta$, and the displacement thickness~$\delta^*$.
		\item Explain shortly (\eg in 30 words or less) how the transition to turbulent regime can be triggered.
		\item Explain shortly (\eg in 30 words or less) how the transition to turbulent regime could instead be delayed.
	\end{enumerate}


%%%%
\subsubsection{Shear force due to boundary layer}
\wherefrom{White \smallcite{white2008} E7.3}
\label{exo_bl_shear}

	A thin and smooth plate of dimensions~$\num{0,5} \times \SI{3}{\metre}$ is placed with a zero angle of attack in a flow incoming at~\SI{1,25}{\metre\per\second}, as shown in \cref{fig_plate_shear_calculations}.
	
		\begin{figure}[h!]
			\begin{center}
				\includegraphics[width=\textwidth]{plate_shear_calculations.png}
			\end{center}
			\supercaption{A thin plate positioned parallel to an incoming uniform flow. Two configurations are studied in this exercise.}{\wcfile{Plates for boundary layer calculations.svg}{Figure} \cczero \oc}
			\label{fig_plate_shear_calculations}
		\end{figure}\vspace{-1cm}%handmade
	
	\begin{enumerate}
		\item What is the shear force exerted on the top surface of the plate for each of the two configurations shown in \cref{fig_plate_shear_calculations}, when the fluid is air of viscosity $\mu_\text{air} = \SI{1,5e-5}{\pascal\second}$?
		\item What are the shear forces when the fluid is water of viscosity $\mu_\text{water} = \SI{1e-3}{\pascal\second}$?
		\item \textit{[difficult question]} How would these shear efforts evolve if the plate was tilted with an angle of \SI{20}{\degree} relative to the flow?
	\end{enumerate}


%%%%
\subsubsection{Wright Flyer I}
\wherefrom{\cczero \oc}
\label{exo_bl_wright_flyer}

	The \we{Wright Flyer I}, the first airplane capable of sustained controlled flight (1903), was a biplane with a \SI{12}{\metre} wingspan (\cref{fig_wright_flyer}). It had two wings of chord length~\SI{1,98}{\metre} stacked one on top of the other. The wing profile was extremely thin and it could only fly at very low angles of attack. Its flight speed was approximately \SI{40}{\kilo\metre\per\hour}.
		\begin{figure}[h!]
			\begin{center}
				\includegraphics[width=0.4\textwidth]{Kitty_hawk_gross}
			\end{center}\vspace{-0.5cm}%handmade
			\supercaption{The \we{Wright Flyer I}, first modern airplane in history. Built with meticulous care and impeccable engineering methodology by two bicycle makers, it made history in December 1903.}{\wcfile{Kitty hawk gross.jpg}{Photo} by Orville Wright, 1908 (\pd)}
			\label{fig_wright_flyer}
		\end{figure}
	
	\begin{enumerate}
		\item If the flow over the wings can be treated as if they were flat plates, what is the power necessary to compensate the shear exerted by the airflow on the wings during flight?
		\item Which other forms of drag would also be found on the aircraft? (give a brief answer, \eg in 30 words or less)
	\end{enumerate}


%%%%
\subsubsection{Power lost to shear on an airliner fuselage}
\wherefrom{\cczero \oc}
\label{exo_bl_friction_fuselage}

	An \we{Airbus A340-600} (fig.~\ref{fig_346}) is cruising at $\ma=\num{0,82}$ at an altitude of \SI{10 000}{\metre} (where the air has viscosity \SI{1,457e-5}{\newton\second\per\metre\squared}, temperature \SI{220}{\kelvin}, density \SI{0,4}{\kilogram\per\metre\cubed}).

		\begin{figure}[h!]
			\begin{center}
				\includegraphics[width=0.4\textwidth]{EC-IOB}
			\end{center}\vspace{-0.5cm}%handmade
			\supercaption{The \we{Airbus A340-600}, a large airliner first flown in 2001.}{\wcfile{EC-IOB.jpg}{Photo} \ccbysa by Iberia Airlines (retouched)}
			\label{fig_346}
		\end{figure}
	
	The cylindrical part of the fuselage has diameter~\SI{5,6}{\metre} and length~\SI{65}{\metre}.
	
	\begin{enumerate}
		\item What is approximately the maximum boundary layer thickness around the fuselage?
		\item What is approximately the average shear applying on the fuselage skin?
		\item Estimate the power dissipated to friction on the cylindrical part of the fuselage.
		\item In practice, in which circumstances could flow separation occur on the fuselage skin? (give a brief answer, \eg in 30 words or less)
	\end{enumerate}


\begin{comment}
%%%%
\subsubsection{Separation according to Pohlhausen}
\wherefrom{non-examinable, based on Richecœur 2012~\cite{richecoeur2012}}
\label{exo_bl_separation_pohlhausen}

	Air at \SI{1}{\bar} and \SI{20}{\degreeCelsius} flows along a smooth surface, and decelerates slowly, with a constant rate of $\SI{-0,25}{\metre\per\second\per\metre}$.
	
	According to the Pohlhausen model (eq.~\ref{eq_modele_Pohlhausen} p.~\pageref{eq_modele_Pohlhausen}), at which distance downstream will separation occur? Is the boundary layer still laminar then?
	
	How could one generate such a deceleration in practice?
\end{comment}

%%%%
\subsubsection{Laminar wing profile}
\wherefrom{non-examinable. Based on a diagram from Bertin et al. 2010~\cite{bertincummings2010}}

	The characteristics of a so-called “laminar” wing profile are compared in \cref{fig_laminar_profile_bertin_one,fig_laminar_profile_bertin_two,fig_laminar_profile_bertin_three} with those of an ordinary profile.

			\begin{figure}
				\begin{center}
					\includegraphics[width=\textwidth]{bertin_laminar_profile_1.png}
				\end{center}
				\supercaption{Comparison of the thickness distribution of two uncambered wing profiles: an ordinary medium-speed \textsc{naca} 0009 profile, and a “laminar” \textsc{naca} 66-009 profile.}{Figure \copyright\xspace Bertin \& Cummings 2010~\cite{bertincummings2010}}
				\label{fig_laminar_profile_bertin_one}
			\end{figure}

			\begin{figure}
				\begin{center}
					\includegraphics[width=0.85\textwidth]{bertin_laminar_profile_2.png}
				\end{center}
				\supercaption{Static pressure distribution (represented as a the local non-dimensional \vocab{pressure coefficient} $C_p \equiv \frac{p -p_\infty}{\frac{1}{2} \rho V^2}$) as a function of distance $x$ (non-dimensionalized with the chord $c$) over the surface of the two airfoils shown in \cref{fig_laminar_profile_bertin_one}.}{Figure \copyright\xspace Bertin \& Cummings 2010~\cite{bertincummings2010}}
				\label{fig_laminar_profile_bertin_two}
			\end{figure}

			\begin{figure}
				\begin{center}
					\includegraphics[width=0.75\textwidth]{bertin_laminar_profile_3.png}
				\end{center}
				\supercaption{Values of the section drag coefficient $C_d \equiv \frac{d}{\frac{1}{2} c \rho V^2}$ as a function of the section lift coefficient $C_l \equiv \frac{l}{\frac{1}{2} c \rho V^2}$ for both airfoils presented in \cref{fig_laminar_profile_bertin_one}.}{Figure \copyright\xspace Bertin \& Cummings 2010~\cite{bertincummings2010}, based on data by Abott \& Von Doenhoff 1949~\cite{abbottvondoenhoff1959}}
				\label{fig_laminar_profile_bertin_three}
			\end{figure}

	On the graph representing the pressure coefficient~$C_p \equiv \frac{p -p_\infty}{\frac{1}{2} \rho V^2}$, identify the curve corresponding to each profile.
	
	What advantages and disadvantages does the laminar wing profile have, and how can they be explained? In which applications will it be most useful?


%%%%
\subsubsection{Separation mechanism}
\wherefrom{non-examinable, \cczero \oc}

	Sketch the velocity profile of a laminar or turbulent boundary layer shortly upstream of, and at a separation point.
	
	The two equations below describe flow in laminar boundary layer:
			\begin{IEEEeqnarray}{rCl}
				u \partialderivative{u}{x} + v \partialderivative{u}{y} & = & U \frac{\diff U}{\diff x} + \frac{\mu}{\rho} \secondpartialderivative{u}{y} \ztag{\ref{eq_ns_bl_lam_un}}\\
			\partialderivative{u}{x} + \partialderivative{v}{y} & = & 0 \ztag{\ref{eq_ns_bl_lam_deux}}
		\end{IEEEeqnarray}
	
	Identify these two equations, list the conditions in which they apply, and explain shortly (\eg in 30 words or less) why a boundary layer cannot separate when a favorable pressure gradient is applied along the~wall.

\begin{comment}
%%%%
\subsubsection{Thwaites’ separation model}
\wherefrom{non-examinable, from White \smallcite{white2008} E7.5}
\label{exo_bl_separation_model}
	
	In 1949, Bryan Thwaites explored the limits of the Pohlhausen separation model. He proposed a different model to describe the laminar boundary layer velocity profile, which is articulated upon an expression for the momentum thickness $\theta$ and is more accurate than the Reynolds-number based descriptions that we have studied. Thwaites proposed the~model:
		\begin{IEEEeqnarray}{rCl}
			\theta^2 = \theta_0^2 \left(\frac{U_0}{U}\right)^6 + \num{0,45}\frac{\mu}{\rho} \frac{1}{U^6} \int_0^x U^5 \diff x
		\end{IEEEeqnarray}
		\begin{equationterms}
			\item in which the $0$-subscripts denote initial conditions (usually $\theta_0 = 0$)
		\end{equationterms}
	
	Thwaites also defined a non-dimensional momentum thickness $\lambda_{\theta}$:
		\begin{IEEEeqnarray}{rCl}
			\lambda_{\theta} &\equiv& \theta^2 \frac{\rho}{\mu} \frac{\diff U}{\diff x}
		\end{IEEEeqnarray}
	and then went on to show that for the model above, separation ($c_f = 0$) occurs when $\lambda_\theta = \num{-0,09}$.
	
	A classical model for flow deceleration is the Howarth longitudinal velocity profile $U_{(x)} = U_0 \left(1 - \frac{x}{L}\right)$, in which $L$ is a reference length of choice. In this velocity distribution with linear deceleration, what is the distance at which the Thwaites model predicts the boundary layer separation?
\end{comment}

\clearpage%%%%
\subsubsection*{Answers}
\NumTabs{1}
\begin{description}
	\item [\ref{exo_water_air_flow}]%
				\tab 1) At trailing edge $\rex = \num{5348}$ thus the layer is laminar everywhere. $\delta$ will grow from~0 to~\SI{2,01}{\centi\metre} (eq.~\ref{eq_delta_lam} p.~\pageref{eq_delta_lam});
					\tab 2) For water: $\delta_\text{trailing edge} = \SI{4,91}{\milli\metre}$.
	\item [\ref{exo_bl_sketches}]%
				\tab 1) See \cref{fig_bl_transition} p.~\pageref{fig_bl_transition}. At the leading-edge the velocity is uniform. Note that the $y$-direction is greatly exaggerated, and that the outer velocity~$U$ is identical for both regimes;
				\tab 2) See \cref{fig_bl_deltastar} p.~\pageref{fig_bl_deltastar}. Note that streamlines penetrate the boundary layer;
				\tab 3) and 4) See \S\ref{ch_bl_transition} p.~\pageref{ch_bl_transition}.
	\item [\ref{exo_bl_shear}]%
				\tab $x_\text{transition, air} = \SI{4,898}{\metre}$ and $x_\text{transition, water} = \SI{0,4}{\metre}$. In a laminar boundary layer, inserting equation~\ref{eq_cf_lam} into equation~\ref{eq_def_shear_coeff} into equation~\ref{eq_force_tau} yields\\
				$F_\tau = \num{0,664} L U^{\num{1,5}} \sqrt{\rho \mu} \left[\sqrt{x}\right]_{0}^{x_\text{transition}}$.\\
	In a turbulent boundary layer, we use equation~\ref{eq_cf_turb} instead and get \\
	$F_\tau = \num{0,01575} L \rho^\frac{6}{7} U^\frac{13}{7} \mu^\frac{1}{7} \left[x^\frac{6}{7}\right]_{x_\text{transition}}^{x_\text{trailing edge}}$. These expressions allow the calculation of the forces below, for the top surface of the plate:
				\tab\tab 1) (air) First case: $F = \SI{3,445e-3}{\newton}$; second case $F = \SI{8,438e-3}{\newton}$ (who would have thought eh?);
				\tab\tab 2) (water) First case: $F = \SI{3,7849}{\newton}$; second case $F = \SI{2,7156}{\newton}$.
	\item [\ref{exo_bl_wright_flyer}]%
				\tab Using the expressions developed in exercise \ref{exo_bl_shear}, $\dot W_\text{friction} \approx \SI{255}{\watt}$.
	\item [\ref{exo_bl_friction_fuselage}]%
				\tab 1) $x_\text{transition} = \SI{7,47}{\centi\metre}$ (the laminar part is negligible). With the equations developed in exercise 7.3, we get $F = \SI{24,979}{\kilo\newton}$ and $\dot W = \SI{6,09}{\mega\watt}$. Quite a jump from the Wright Flyer I!
				\tab\tab 2) When the longitudinal pressure gradient is zero, the boundary layer cannot separate. Thus separation from the fuselage skin can only happen if the fuselage is flown at an angle relative to the flight direction (\eg during a low-speed maneuver).
%	\item [\ref{exo_bl_separation_model}]%
%				\tab Once the puzzle pieces are put together, this is an algebra exercise: $\left(\frac{x}{L}\right)_\text{separation} = \num{0,1231}$. Bryan beats Ernst!
\end{description}
\atendofexercises
