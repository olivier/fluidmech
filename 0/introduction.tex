\newgeometry{left=3cm,right=3cm,top=3cm,bottom=2cm,marginparwidth=0cm,marginparsep=0cm}
\setlength{\fancyfootoffset}{1cm}%
\fancyfoot{}

\begin{center}
\phantomsection
\addcontentsline{toc}{chapter}{Contents}
	\includegraphics[width=\textwidth]{course_layout}
\end{center}

\clearpage

\dominitoc % not actually printing a minitoc, just preparing minitocs for later inclusion
\hypersetup{linkcolor=black}				% make links black
\tableofcontents
\hypersetup{linkcolor=externallinkcolor}	% back to normal link color
\clearpage

\phantomsection
\addcontentsline{toc}{chapter}{About this course (syllabus)}

~\vspace{2cm}
\begin{center}
	{\LARGE About this course (syllabus)}\par
	
	Fluid dynamics for engineers by Olivier Cleynen, PhD\\
	University of Magdeburg, 2015-2020\\
	\href{https://fluidmech.ninja/}{https://fluidmech.ninja/}
	
	
\vspace{2em}
\end{center}

Welcome! From 2015 to 2020, these notes were the core of the Fluid Dynamics course of the \textit{Chemical and Energy Engineering} program at the University of Magdeburg, Germany. 

\section*{Objectives}

	Starting with little or no experience with fluid mechanics, after taking this course:
	\begin{itemize}
		\item you should have a good understanding of what can, and cannot, be calculated with fluid mechanics in engineering: how we approach problems depending on how much information is available.
		\item you should be able to solve several real-world engineering fluid mechanics problems with confidence: calculating forces within fluids and on objects, predicting flow in pipes, near walls, at small and large scales.
	\end{itemize}

	My objective is to enable you to get there with the minimum amount of your time and energy (but not minimum power!).
	
	If all goes well, at the end of the semester, you should be well-prepared to begin a course in \vocab{Computational Fluid Dynamics}, where the knowledge and skills you acquire here can be used to solve applied problems in great detail.


\section*{About these notes and the author}

	\begin{wrapfigure}[7]{R}[0pt]{0.4\textwidth}\vspace{-\intextsep}\includegraphics[width=0.4\textwidth]{olivier}\vspace{-\intextsep}\end{wrapfigure}
	My name is \href{https://ariadacapo.net/}{Olivier Cleynen}. I had the pleasure and priviledge of directing this course from 2015 to 2020 while I was a PhD student at the University Otto von Guericke of Magdeburg. In 2021, my colleagues at the \href{https://www.lss.ovgu.de/}{fluid dynamics laboratory} took over. I am no longer involved in teaching the course, but am delighted that these course materials live on.
	
	If you are a student enrolled in this course, you should definitely stop reading now and check with your course instructor so you understand how the course will go (where to get help, what chapters are examinable, and so on). I am not at all up-to-date with that information, and unfortunately cannot help at all!
	
	I obtained my Master’s in 2006, then went on to found and work for a non-profit organization, and then became a university teacher in France. I arrived in Magdeburg in 2015, and currently live here with my partner and her twelve year-old child. I completed my PhD in 2022, and I now am a math and physics teacher in a local high school. I am delighted that the present course notes at \href{https://fluidmech.ninja/}{https://fluidmech.ninja/} keep being useful to students around the world.
	


\section*{Contact}
\label{ch_contact}


	My email is olivier\raisebox{-0.2ex}{\includegraphics[width=1.7ex]{arobase}}cleynen.fr. I am available to answer queries about re-using the course materials. Unfortunately, I cannot provide assistance with the content itself (\eg how to solve the end-of-chapter problems) or answer any questions about the course. If you would like such assistance, please contact my colleagues at the \href{https://www.lss.ovgu.de/}{fluid dynamics laboratory} of the University of Magdeburg.


\section*{Copyright, remixing, and authors}

	This document is mainly authored by \href{https://ariadacapo.net/}{Olivier Cleynen}. Substantial contributions have been made by colleagues Germán Santa-Maria~\cite{cleynenetal2020peergraded}, Jochen König, and Arjun Neyyathala. Numerous improvements have been contributed by students over the years.\\
	Many figures from authors not asso	ciated with this course are included; the author, license, and a link to the source are indicated every time. A few figures still remain which are extracted from cited, fully-copyrighted works, as indicated.\\
	Most portrait illustrations are authored by Oksana Grivina; they are fully-copyrighted and used under a commercial license in this project. Other portrait illustrations are authored by Agustin Dede Prasetyo and Olivier Cleynen under a \ccby license.
	
	The text of this document is licensed under a Creative Commons \ccbync (attribution, non-commercial) license. This means you are free to make copies of it, but not for commercial purposes. This license also does not permit uploading this document to content platforms such as Facebook, Academia.edu or Scribd. The Latex sources of this document can be accessed from the git repository accessed from the course homepage.~\cite{cleynen2020fluidmechgit} A number of YouTube videos, shot during the 2020 German lockdown, are linked along the text.
	
	If you use this document in other works, please cite it as “Olivier Cleynen. \textit{Fluid dynamics for engineers}. Under \ccbync license. 2020. \textsc{url}: \href{https://fluidmech.ninja/}{\textcolor{black}{https://fluidmech.ninja}}”.

\section*{Conclusion}

\begin{center}
	\href{https://youtu.be/NJfiIGOADK0}{\includegraphics[width=0.6\textwidth]{welcome_video}}

	\textit{\href{https://youtu.be/NJfiIGOADK0}{Welcome to this course!}}\\
	(I recorded this YouTube video in April 2020)\par
\end{center}

I hope you have a great semester! Fluid mechanics is one of the most exciting disciplines out there. Now, let us begin!
	
	\begin{flushright}
	Olivier Cleynen\\
	April 2020 (updated October 2022)
	\end{flushright}

\defaultfooter
\restoregeometry\restoredefaultfootoffset\cleardoublepage
