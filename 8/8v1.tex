\documentclass[17pt]{beamer}
\usepackage{fluidmechslides} % from https://framagit.org/olivier/sensible-styles

% Syntax for single-image slides:
% (the first argument (number) being the maximum fraction of the
%  slide width that the image is allowed to have.)
% \figureframe{1}{filename}{Title}{Attribution}

% Do I want the print version? (no \pause, larger preamble)
\printversion

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\sectionstarwithsubtitle{Comparing influences}{miniature physics}

	\subsectionstar{Principle}
	
		\begin{frame}
			How do you investigate:
			\begin{itemize}\pause
				\item Six different configurations of a \SI{100}{\metre} long airplane?\pause
				\item How a mosquito flies?\pause
				\item Liquid metal in a furnace?\pause
				\item Blood flow in a beating heart?
			\end{itemize}
		\end{frame}
		
		\begin{frame}{Answer:}
			\begin{centering}
			
			Flows are \vocab{dynamically similar}
			
			when their \vocab{flow parameters} are the same
		
			\end{centering}
		\end{frame}
			

	\subsectionstarwithsubtitle{The non-dimensional Navier-Stokes equation}{but there is \textit{no}\\ non-dimensional Bernoulli equation}


	\begin{frame}
		All cool things begin by writing the Navier-Stokes equation:\pause
			\begin{IEEEeqnarray*}{rCl}
				\rho \partialtimederivative{\vec V} + \rho \advective{\vec V}  & = &	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		Now, we create dimensionless terms:
			\begin{IEEEeqnarray*}{rCl}\pause
				 t^* 			&\equiv& ft\\
				 \vec V^* 	&\equiv& \frac{\vec V}{V}\\
				 p^*			&\equiv& \frac{p - p_\infty}{p_0 - p_\infty}\\
				 \vec g^*			&\equiv& \frac{\vec g}{g}\\
				 \gradient^* &\equiv& L \gradient
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		What is a non-dimensional term?\pause
			\begin{IEEEeqnarray*}{rCl}
				\vec A &=& A \ \ \vec A^*
			\end{IEEEeqnarray*}\pause
		$\to$ replace vector\\
			by scalar $\times$ unit vector
	\end{frame}
	
	\begin{frame}
		Non-dimensional time\pause
			\begin{IEEEeqnarray*}{rCl}
				 t^* 				&\equiv& f \ t
			\end{IEEEeqnarray*}\pause
		$t^*$ runs from 0 to 1, within the period $\frac{1}{f}$
		
		\small 	High frequency: highly-unsteady\\
				Low frequency: quasi-steady
	\end{frame}
	
	\begin{frame}
		Non-dimensional velocity vector field\pause
			\begin{IEEEeqnarray*}{rCl}
				 \vec V^* 		&\equiv& \frac{\vec V}{V}
			\end{IEEEeqnarray*}\pause
		$\vec V^*$ is a unit vector field!
	\end{frame}
	
	\begin{frame}
		Non-dimensional pressure field\pause
			\begin{IEEEeqnarray*}{rCl}
				 p^*				&\equiv& \frac{p - p_\infty}{p_0 - p_\infty}
			\end{IEEEeqnarray*}\pause
		Subtract $p_\infty$ (since only pressure \emph{differences} matter in fluid mech);\\\pause
		Normalize with reference $(p_0 - p_\infty)$
	\end{frame}
	
	\begin{frame}
		Non-dimensional gravity\pause
			\begin{IEEEeqnarray*}{rCl}
 				 \vec g^*		&\equiv& \frac{\vec g}{g}
			\end{IEEEeqnarray*}\pause
		$\vec g^*$ is a unit vector (field)
	\end{frame}
	
	\begin{frame}
		Non-dimensional math?\\\pause
		(non-dimensional del)
			\begin{IEEEeqnarray*}{rCl}\pause
				 \gradient^* 	&\equiv& L \ \gradient
			\end{IEEEeqnarray*}\pause
		Normalize fields with reference length $L$
	\end{frame}
	
	\begin{frame}
		Insert spooky terms in ordinary terms
			\begin{IEEEeqnarray*}{rCl}
 				 t 				&=& \frac{t^*}{f}\\\pause
				 \vec V	 		&=& V \ \vec V^*\\\pause
				 p - p_\infty	&=& p^* \ (p_0 - p_\infty)\\\pause
				 \vec g			&=& g \ \vec g^*\\\pause
				 \gradient	 	&=& \frac{1}{L} \gradient^*
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		And now, let’s re-write!\pause
			\begin{IEEEeqnarray*}{lll}
				\rho \partialtimederivative{\vec V} + \rho \advective{\vec V}  =	\rho \vec g - \gradient{p} + \mu \laplacian{\vec V}\nonumber\\\pause
				\rho  \partialderivative{}{\frac{1}{f} t^*} \left(V \vec V^*\right) + \rho \left(V \vec V^* \cdot \frac{1}{L} \gradient^* \right) \left(V \vec V^*\right)  \nonumber\\\pause
				= \rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty) + p_\infty\right] + \mu \frac{\gradient^{*2}}{L^2} \left(V \vec V^*\right)\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		…and keep going…
			\begin{IEEEeqnarray*}{lll}
				\rho  \partialderivative{}{\frac{1}{f} t^*} \left(V \vec V^*\right) + \rho \left(V \vec V^* \cdot \frac{1}{L} \gradient^* \right) \left(V \vec V^*\right)  \nonumber\\
				= \rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty) + p_\infty\right] + \mu \frac{\gradient^{*2}}{L^2} \left(V \vec V^*\right)\nonumber\\\pause
				\rho V f \partialderivative{\vec V^*}{t^*} + \rho V V \frac{1}{L} \left(\vec V^* \cdot \gradient^*\right) \vec V^* \nonumber\\
				= \rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty)\right] + \mu V \frac{1}{L^2}\gradient^{*2} \vec V^*\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		…and going…
			\begin{IEEEeqnarray*}{lll}
				\rho V f \partialderivative{\vec V^*}{t^*} + \rho V V \frac{1}{L} \left(\vec V^* \cdot \gradient^*\right) \vec V^* \nonumber\\
				= \rho g \ \vec g^* - \frac{1}{L} \gradient^* \left[p^* \ (p_0 - p_\infty)\right] + \mu V \frac{1}{L^2}\gradient^{*2} \vec V^*\nonumber\\\pause
				\left[\rho V f\right]\ \partialderivative{\vec V^*}{t^*} + \left[\frac{\rho V^2}{L}\right]\ \left(\vec V^* \cdot \gradient^*\right) \vec V^*  \nonumber\\\pause
				= \left[\rho g\right]\ \vec g^* - \left[\frac{p_0 - p_\infty}{L}\right]\ \gradient^* p^* + \left[\frac{\mu V}{L^2}\right]\ \gradient^{*2} \vec V^*\nonumber\\\label{eq_nsndtmp}
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{frame}
		An equation with terms in \si{\kilogram\per\metre\squared\per\second\squared}\\\pause
		pondering {\color{nicegreen}unit vectors}\pause
			\begin{IEEEeqnarray*}{lll}
				\left[\rho V f\right]\ {\color{nicegreen}\partialderivative{\vec V^*}{t^*}} + \left[\frac{\rho V^2}{L}\right]\ {\color{nicegreen}\left(\vec V^* \cdot \gradient^*\right) \vec V^*}  \nonumber\\
				= \left[\rho g\right]\ {\color{nicegreen}\vec g^*} - \left[\frac{p_0 - p_\infty}{L}\right]\ {\color{nicegreen}\gradient^* p^*} + \left[\frac{\mu V}{L^2}\right]\ {\color{nicegreen}\gradient^{*2} \vec V^*}\nonumber\\\ztag{\ref{eq_nsndtmp}}
			\end{IEEEeqnarray*}
	\end{frame}
		
	\begin{frame}
		Multiplying by $\frac{L}{\rho V^2}$,
			\begin{IEEEeqnarray*}{rCl}
				&&\left[\rho V f\right]\ \partialderivative{\vec V^*}{t^*} + \left[\frac{\rho V^2}{L}\right]\ (\vec V^* \cdot \gradient^*) \vec V^*  \nonumber\\
				&&= \left[\rho g\right]\ \vec g^* - \left[\frac{p_0 - p_\infty}{L}\right]\ \gradient^* p^* + \left[\frac{\mu V}{L^2}\right]\ \gradient^{*2} \vec V^*\nonumber\\\ztag{\ref{eq_nsndtmp}}\\\pause
				&&\left[{\color{niceblue}\frac{fL}{V}}\right]\ \partialderivative{\vec V^*}{t^*} + \left[{\color{niceblue}1}\right]\ (\vec V^* \cdot \gradient^*) \vec V^*  \nonumber\\
				&&= \left[{\color{niceblue}\frac{gL}{V^2}}\right]\ \vec g^* - \left[{\color{niceblue}\frac{p_0 - p_\infty}{\rho V^2}}\right]\ \gradient^* p^* + \left[{\color{niceblue}\frac{\mu}{\rho V L}}\right]\ \gradient^{*2} \vec V^*\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}


	\begin{frame}
		The Four Horsemen of the Apocalypse\pause
			\begin{IEEEeqnarray*}{rCl}
				 \str &\equiv& \frac{f\ L}{V}\\\pause
				 \eu &\equiv& \frac{p_0 - p_\infty}{\rho\ V^2}\\\pause
				 \fr &\equiv& \frac{V}{\sqrt{g\ L}}\\\pause
				 \re &\equiv& \frac{\rho\ V\ L}{\mu}
			\end{IEEEeqnarray*}
	\end{frame}
	
	\begin{comment}
	\begin{frame}
		A somewhat nicer skin:\pause
			\begin{IEEEeqnarray*}{l}
				\str\ \partialderivative{\vec V^*}{t^*} + [1]\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\
				= \left[\frac{1}{\text{Fr}^2}\right]\ \vec g^* - \eu\ \gradient^* p^* + \left[\frac{1}{\text{Re}}\right]\ \gradient^{*2} \vec V^*\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}
	\end{comment}
	
	\begin{frame}
		And now finally!\pause
			\begin{IEEEeqnarray*}{l}
				{\color{niceblue}\str}\ \partialderivative{\vec V^*}{t^*} + {\color{niceblue}[1]}\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\\pause
				= \frac{1}{{\color{niceblue}\fr}^2}\ \vec g^* - {\color{niceblue}\eu}\ \gradient^* p^* + \frac{1}{{\color{niceblue}\re}}\ \gradient^{*2} \vec V^*\nonumber\label{eq:ns_nondim}\\
			\end{IEEEeqnarray*}\pause
			\vocab{The Glorious Nondimensional Incompressible Navier-Stokes Equation}
	\end{frame}


	\begin{frame}
		Isn’t this great?\pause
		\begin{enumerate}
			\item We can quantify the \textbf{relative weight} of the terms.\\\pause
					$\to$ Which ones can we neglect?	
		\end{enumerate}
	\end{frame}
	
	\begin{frame}
		Isn’t this great?\pause
		\begin{enumerate}
			\shift{1}
			\item We know what to do to obtain \textbf{dynamic similarity} between two scales.\\\pause
					$\to$ Want identical $\vec V^*$ field? We need identical {\color{niceblue}\str}, {\color{niceblue}\eu}, {\color{niceblue}\fr} and {\color{niceblue}\re}.
		\end{enumerate}
	\end{frame}

	\begin{comment}
	\begin{frame}
			\begin{IEEEeqnarray*}{l}
				{\color{niceblue}\str}\ \partialderivative{\vec V^*}{t^*} + {\color{niceblue}[1]}\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\\pause
				= \frac{1}{{\color{niceblue}\fr}^2}\ \vec g^* - {\color{niceblue}\eu}\ \gradient^* p^* + \frac{1}{{\color{niceblue}\re}}\ \gradient^{*2} \vec V^*\nonumber\label{eq:ns_nondim}\\
			\end{IEEEeqnarray*}\pause
			\vocab{The Very Totally Glorious Nondimensional Incompressible Navier-Stokes Equation}
	\end{frame}
	
	\begin{frame}
		Have we been introduced?
		\begin{description}\pause
			\item [Je Strouhal number, St] \pause
				\begin{IEEEeqnarray*}{rCl}
					 \str &\equiv& \frac{f\ L}{V}
				\end{IEEEeqnarray*}\pause
				Quantifies the relative importance of unsteadiness in the flow. \\\pause
				\small Very low frequencies $f$ = quasi-steady
		\end{description}
	\end{frame}
	
	\begin{frame}
		Have we been introduced?
		\begin{description}
			\item [Der Euler number, Eu] \pause
				\begin{IEEEeqnarray*}{rCl}
					 \eu &\equiv& \frac{p_0 - p_\infty}{\rho\ V^2}\\\pause
				\end{IEEEeqnarray*} quantifies the relative importance of the pressure change $p_0 - p_\infty$ within the flow.
		\end{description}
	\end{frame}
	
	\begin{frame}
		Have we been introduced?
		\begin{description}
			\item [The Froude number, Fr] \pause
				\begin{IEEEeqnarray*}{rCl}
					 \fr &\equiv& \frac{V}{\sqrt{g\ L}}\\\pause
				\end{IEEEeqnarray*} quantifies the relative importance of gravity effects in the flow.\\\pause
		\end{description}
	\end{frame}
	
	\begin{frame}
		Have we been introduced?
		\begin{description}
			\item [The Reynolds number, Re] \pause
				\begin{IEEEeqnarray*}{rCl}
					 \re &\equiv& \frac{\rho\ V\ L}{\mu}\pause
				\end{IEEEeqnarray*} quantifies the relative importance of shear in the flow.\\\pause
				\small Large $\re$: viscosity plays a negligible role, velocity field is dictated by pressure.
				\end{description}
	\end{frame}
	
	\begin{frame}
		Have we been introduced? (surprise guest)
		\begin{description}\pause
			\item [Der Mach Nummer]
				\begin{IEEEeqnarray*}{rCl}
					\ma &\equiv& V/a
				\end{IEEEeqnarray*} quantifies the relative importance of compression/expansion in flow.
		\end{description}
	\end{frame}
	\end{comment}

	\begin{frame}
			\begin{IEEEeqnarray*}{l}
				{\color{niceblue}\str}\ \partialderivative{\vec V^*}{t^*} + {\color{niceblue}[1]}\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\
				= \frac{1}{{\color{niceblue}\fr}^2}\ \vec g^* - {\color{niceblue}\eu}\ \gradient^* p^* + \frac{1}{{\color{niceblue}\re}}\ \gradient^{*2} \vec V^*\nonumber\label{eq:ns_nondim}\\
			\end{IEEEeqnarray*}
	\end{frame}
		
	\begin{frame}
			\begin{IEEEeqnarray*}{rCl}
				&&\left[{\color{niceblue}\frac{fL}{V}}\right]\ \partialderivative{\vec V^*}{t^*} + \left[{\color{niceblue}1}\right]\ (\vec V^* \cdot \gradient^*) \vec V^*  \nonumber\\
				&&= \left[{\color{niceblue}\frac{gL}{V^2}}\right]\ \vec g^* - \left[{\color{niceblue}\frac{p_0 - p_\infty}{\rho V^2}}\right]\ \gradient^* p^* + \left[{\color{niceblue}\frac{\mu}{\rho V L}}\right]\ \gradient^{*2} \vec V^*\nonumber\\
			\end{IEEEeqnarray*}
	\end{frame}

	\begin{frame}
			\begin{IEEEeqnarray*}{l}
				{\color{niceblue}\str}\ \partialderivative{\vec V^*}{t^*} + {\color{niceblue}[1]}\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\
				= \frac{1}{{\color{niceblue}\fr}^2}\ \vec g^* - {\color{niceblue}\eu}\ \gradient^* p^* + \frac{1}{{\color{niceblue}\re}}\ \gradient^{*2} \vec V^*\nonumber\label{eq:ns_nondim}\\
			\end{IEEEeqnarray*}\pause
			\vocab{The tattoo-it-on-your-arm Nondimensional Incompressible Navier-Stokes Equation}
	\end{frame}

	\skipinprint{
	\begin{frame}
			\begin{IEEEeqnarray*}{l}
				{\color{niceblue}\str}\ \partialderivative{\vec V^*}{t^*} + {\color{niceblue}[1]}\ \vec V^* \cdot \gradient^* \vec V^*  \nonumber\\
				= \frac{1}{{\color{niceblue}\fr}^2}\ \vec g^* - {\color{niceblue}\eu}\ \gradient^* p^* + \frac{1}{{\color{niceblue}\re}}\ \gradient^{*2} \vec V^*\nonumber\label{eq:ns_nondim}\\
			\end{IEEEeqnarray*}\pause
			The SPHINX\\\pause
			\vocab{Some Pretty Himportant Iquation, No Xaggeration}
	\end{frame}}

\end{document}
