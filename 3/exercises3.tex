\renewcommand{\lastedityear}{2020}
 \renewcommand{\lasteditmonth}{05}
   \renewcommand{\lasteditday}{15}
\renewcommand{\numberofthischapter}{3}
\renewcommand{\titleofthischapter}{\namechapterthreetitle}
\atstartofexercises

\fluidmechexercisestitle


\mecafluexboxen

\begin{boiboiboite}
Mass balance through an arbitrary volume:
	\begin{IEEEeqnarray}{rCCCl}
		0 & = & \timederivative{} \iiint_\cv \rho \diff \vol  & + & \iint_\cs \rho \ (\vec V_\rel \cdot \vec n) \diff A \ztag{\ref{eq_rtt_mass}}
	\end{IEEEeqnarray}

Momentum balance through an arbitrary volume:
	\begin{IEEEeqnarray}{rCCCl}
		\vec F_\net & = & \timederivative{} \iiint_\cv \rho \vec V \diff \vol  & + & \iint_\cs \rho \vec V \ (\vec V_\rel \cdot \vec n) \diff A \ztag{\ref{eq_rtt_linearmom}}
	\end{IEEEeqnarray}

Angular momentum balance through an arbitrary volume:
	\begin{IEEEeqnarray}{rCCCl}
		\vec M_{\net, \X} &=& \timederivative{} \iiint_\cv \vec r_{\X m} \wedge \rho \vec V \diff \vol  &+& \iint_\cs \vec r_{\X m} \wedge \rho \ (\vec V_\rel \cdot \vec n) \vec V \diff A \ztag{\ref{eq_rtt_angularmom}}
	\end{IEEEeqnarray}
\end{boiboiboite}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reading quiz}

	\quiz

(the quiz for chapter 3 will open from May 10 to May 20)

%%%%
\subsubsection{Pipe bend}
\wherefrom{\cczero \oc}
\label{exo_pipe_bend}

	A pipe with diameter~\SI{30}{\milli\meter} has a bend with angle $\theta = \SI{130}{\degree}$, as shown in \cref{fig_pipe_bend}. Water enters and leaves the pipe with the same speed $V_1 = V_2 = \SI{1,5}{\metre\per\second}$. The velocity distribution at both inlet and outlet is uniform.
	\begin{figure}[ht!]
		\begin{center}
		\includegraphics[width=7.5cm]{pipe_bend}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A pipe bend, through which water is flowing. We assume that the velocity distribution at inlet and outlet is identical.}{\wcfile{Pipe bend schematic.svg}{Figure} \cczero \oc}
		\label{fig_pipe_bend}
	\end{figure}
	\begin{enumerate}
		\item What is the mass flow traveling through the pipe?
		\item What is the force exerted by the pipe bend on the water?
		\item Represent the force vector qualitatively (\ie without numerical data).
		\item What would be the new force if all of the speeds were doubled?
	\end{enumerate}

%%%%
\subsubsection{Exhaust gas deflector}
\label{exo_exhaust_gas_deflector}

	A deflector is used behind a stationary aircraft during ground testing of a jet engine (\cref{fig_deflector}).	
	\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=11cm]{deflector}
	\end{center}\vspace{-0.5cm}%handmade
		\supercaption{A mobile exhaust gas deflector, used to deflect hot jet engine exhaust gases upwards during ground tests.}{\wcfile{Exhaust gas deflector 2.svg}{Figure} \cczero \oc}
		\label{fig_deflector}
	\end{figure}\vspace{-0.5cm}%handmade
	
	The deflector is fed with a horizontal air jet with a quasi-uniform velocity profile; the speed is~$V_\text{jet} = \SI{600}{\kilo\metre\per\hour}$, temperature~\SI{400}{\degreeCelsius} and the pressure is atmospheric. As the exhaust gases travel through the pipe, their heat losses are negligible. Gases are rejected with a~\SI{40}{\degree} angle relative to the horizontal.
	
	The inlet diameter is~\SI{1}{\metre} and the horizontal outlet surface is~\SI{6}{\metre\squared}.
	
	\begin{enumerate}
		\item What is the force exerted on the ground by the deflection of the exhaust gases?
		\item Describe qualitatively (\ie without numerical data) a modification to the deflector that would reduce the horizontal component of force.
		\item What would the force be if the deflector traveled rearwards (positive $x$-direction) with a velocity of~\SI{10}{\metre\per\second}?
	\end{enumerate}


%%%%
\subsubsection{Pelton water turbine}
\wherefrom{White \smallcite{white2008} P3.56}
\label{exo_pelton_turbine}

	A water turbine is modeled as the following system: a water jet exiting a stationary nozzle hits a blade which is mounted on a rotor (\cref{fig_water_turbine}). In the ideal case, viscous effects can be neglected, and the water jet is deflected entirely with a~\SI{180}{\degree} angle.
	\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=12cm]{water_turbine_blade}
		\end{center}
		\supercaption{Schematic drawing of a water turbine blade. This type of turbine is called \we{Pelton turbine}.}{\wcfile{Nozzle flow case 3.svg}{Figure} \cczero \oc}
		\label{fig_water_turbine}
	\end{figure}
	
	The nozzle has a cross-section diameter of~\SI{5}{\centi\metre} and produces a water jet with a speed $V_\text{jet}=\SI{15}{\metre\per\second}$. The rotor diameter is~\SI{2}{\metre} and the blade height is negligibly small.
	
	We first study the case in which the rotor is prevented from rotating, so that the blade is stationary ($V_\text{blade} = \SI{0}{\metre\per\second}$).
	\begin{enumerate}
		\item What is the force exerted by the water on the blade?
		\item What is the moment exerted by the blade around the rotor axis?
		\item What is the power transmitted to the rotor?
	\end{enumerate}
	
	We now let the rotor rotate freely. Friction losses are negligible, and it accelerates until it reaches maximum velocity.
	\begin{enumerate}
		\shift{3}
		\item What is the rotor rotation speed?
		\item What is the power transmitted to the rotor?
	\end{enumerate}
	
	The rotor is now coupled to an electrical generator.	
	\begin{enumerate}
		\shift{5}
		\item Show that the maximum power that can be transmitted to the generator occurs for $V_\text{blade} = \frac{1}{3} V_\text{water}$.
		\item What is the maximum power that can be transmitted to the generator?
		\item How would the above result change if viscous effects were taken into account? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


%%%%
\subsubsection{Snow plow}
\wherefrom{derived from Gerhart \& Gross~\smallcite{gerhartgross1985} Ex5.9}
\label{exo_snow_plow}

	A road-based snow plow (\cref{fig_snowplow}) is clearing up the snow on a flat surface. We wish to quantify the power required for its operation.
	\begin{figure}
		\begin{center}
		\includegraphics[width=11cm]{bladeplow}
		\vspace{-1cm}
		\end{center}
		\supercaption{Outline schematic of a blade snow plow.}{\wcfile{Snow_blade_truck_conceptual_drawing.svg}{Figure} \cczero \oc}
			\label{fig_snowplow}
	\end{figure}
	
	The snow plow is advancing at~\SI{25}{\kilo\metre\per\hour}; its blade has a frontal-view width of~\SI{4}{\metre}.
	
	The snow on the ground is~\SI{30}{\centi\metre} deep and has density~\SI{300}{\kilogram\per\metre\cubed}.
	
	The snow is pushed along the blade and is rejected horizontally with a~\SI{30}{\degree} angle to the left of the plow.	Its density has then risen to~\SI{450}{\kilogram\per\metre\cubed}. The cross-section area $A_\text{outlet}$ of the outflowing snow in the $x$-$y$ plane is \SI{1,1}{\metre\squared}.
	\begin{enumerate}
		\item What is the force exerted on the blade by the deflection of the snow? \\
				(Indicate its magnitude and coordinates)
		\item What is the power required for the operation of the snow plow?
		\item If the plow velocity was increased by \SI{10}{\percent}, what would be the increase in power?
	\end{enumerate}

%%%%
\subsubsection{Inlet of a pipe}
\label{exo_pressure_losses_pipe_flow}
\wherefrom{Based on White \cite{white2008}}

	Water is circulated inside a cylindrical pipe with diameter \SI{1}{\metre} (\cref{fig_pipe}).
	
	\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.6\textwidth]{pipe_integral_analysis}
		\end{center}
		\supercaption{Velocity profiles at the inlet and outlet of a circular pipe.}{\wcfile{Pipe flow laminar velocity distributions.svg}{Figure} \cczero \oc}
		\label{fig_pipe}
	\end{figure}
	
	At the entrance of the pipe, the speed is uniform: $u_1 = U_\av = \SI{5}{\metre\per\second}$.
	
	Shear applies on the fluid from the walls, where the velocity is zero. This strains the fluid particles, and changes the velocity distribution. At the outlet of the pipe, the velocity profile is no longer uniform. It can be modeled as a function of the radius with the relationship:
		\begin{equation}
			u_{2(r)} = U_\text{center} \left(1 - \frac{r}{R}\right)^{\frac{1}{7}}
		\end{equation}
		
	\begin{comment}
	The momentum flow at the exit may be described solely in terms of the average velocity $U_\av$ with the help of a \vocab{momentum flux correction factor} $\beta$, such that $\rho \int u_2^2 \diff A = \beta \rho A U_\av^2$.\\
		When $u_2 = U_\text{center} \left(1 - \frac{r}{R}\right)^{m}$, it can be shown that $\beta = \frac{(1 + m)^2 (2+m)^2}{2(1 + 2m)(2+2m)}$, and so with $m = \frac{1}{7}$, we have $\beta \approx \num{1,02}$.

	\textit{[difficult question]}\\
	What is the net force applied on the water so that it may travel through the pipe?
	\end{comment}
	
	What is the center velocity $U_\text{center}$ at the outlet?


%\clearpage
%%%%
\subsubsection{Drag on a cylindrical profile}
\label{exo_drag_cylindrical_profile}

	In order to measure the drag on a cylindrical profile, a cylindrical tube is positioned perpendicular to the air flow in a wind tunnel (\cref{fig_cylinder}), and the longitudinal component of velocity is measured across the tunnel section.
	\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{cylinder_integral_analysis}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{A cylinder profile set up in a wind tunnel, with the air flowing from left to right.}{\wcfile{Velocity profiles integral analysis.svg }{Figure} \cczero \oc}
		\label{fig_cylinder}
	\end{figure}

	Upstream of the cylinder, the air flow velocity is uniform ($u_1 = U = \SI{30}{\metre\per\second}$).

	Downstream of the cylinder, the speed is measured across a~\SI{2}{\metre} height interval. Horizontal speed measurements are gathered and modeled with the following relationship:
		\begin{equation}
			u_{2(y)} = 29 + y^2
		\end{equation}
	
	The width of the cylinder (perpendicular to the flow) is~\SI{2}{\metre}. The Mach number is very low, and the air density remains constant at $\rho = \SI{1,23}{\kilogram\per\metre\cubed}$; pressure is uniform all along the measurement field.
	
	\begin{enumerate}
		\item What is the drag force applying on the cylinder?
		\item How would this value change if the flow in the cylinder wake was turbulent, and the function $u_{2(y)}$ above only modeled \emph{time-averaged values} of the horizontal velocity? (briefly justify your answer, \eg in 30 words or less)
	\end{enumerate}


\clearpage
%%%%
\subsubsection{Drag on a flat plate}
\label{exo_drag_flat_plate}

	We wish to measure the drag applying on a thin plate positioned parallel to an air stream. In order to achieve this, measurements of the horizontal velocity $u$ are made around the plate (\cref{fig_platebl}).
	
	\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{plate_integral_analysis}
		\vspace{-0.5cm}
		\end{center}
		\supercaption{Side view of a plate positioned parallel to the flow.}{\wcfile{Velocity profiles integral analysis.svg }{Figure} \cczero \oc}\vspace{-1cm}
		\label{fig_platebl}
	\end{figure}
	
	At the leading edge of the plate, the horizontal velocity of the air is uniform: $u_1 = U = \SI{10}{\metre\per\second}$.
	
	At the trailing edge of the plate, we observe that a thin layer of air has been slowed down by the effect of shear. This layer, called \vocab{boundary layer}, has a thickness of $\delta = \SI{1}{\centi\metre}$. The horizontal velocity profile can be modeled with the relation:
		\begin{equation}
			u_{2(y)} = U \left(\frac{y}{\delta}\right)^{\frac{1}{7}}
		\end{equation}
		
	The width of the plate (perpendicular to the flow) is~\SI{30}{\centi\metre} and it has negligible thickness. The flow is incompressible ($\rho = \SI{1,23}{\kilogram\per\metre\cubed}$) and the pressure is uniform.
	
		\begin{enumerate}
			\item What is the drag force applying on the plate?
			\item What is the power required to compensate the drag? 
			\item Under which form is the kinetic energy lost by the flow carried away? Can this new form of energy be measured? (briefly justify your answer, \eg in 30 words or less)
		\end{enumerate}
	
\clearpage
%%%%
\subsubsection{Drag measurements in a wind tunnel}
\label{exo_drag_wing_profile}

	A group of students proceeds with speed measurements in a wind tunnel. The objective is to measure the drag applying on a wing profile positioned across the tunnel test section (\cref{fig_profile}).
	
	\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.85\textwidth]{profile_integral_analysis}
		\end{center}
		\supercaption{Wing profile positioned across a wind tunnel. The horizontal velocity distributions upstream and downstream of the profile are also shown.}{\wcfile{Velocity profiles integral analysis.svg }{Figure} \cczero \oc}
		\label{fig_profile}
	\end{figure}

	Upstream of the profile, the air flow velocity is uniform ($u_1 = U = \SI{50}{\metre\per\second}$).
	
	Downstream of the profile, horizontal velocity measurements are made every~\SI{5}{\centi\metre} across the flow; the following results are obtained:
	
	\begin{center}
		\begin{tabularx}{10cm}{S|S} % the @{something} kills the inter-column space and replaces it with "something"
		{vertical position (\si{\centi\metre})} & {horizontal speed $u_2$ (\si{\metre\per\second})}\\
		\hline
		0	& 50\\
		5	& 50\\
		10 & 49\\
		15	& 48\\
		20	& 45\\
		25 & 41\\
		30	& 39\\
		35	& 40\\
		40 & 43\\
		45	& 47\\
		50 & 48\\
		55	& 50\\
		60 & 50\\
		\hline
		\end{tabularx}
	\end{center}

	The width of the profile (perpendicular to the flow) is~\SI{50}{\centi\metre}. The airflow is incompressible ($\rho = \SI{1,23}{\kilogram\per\metre\cubed}$) and the pressure is uniform across the measurement surface.
	
	\begin{enumerate}
		\item What is the drag applying on the profile?
		\item How would the above calculation change if \emph{vertical} speed measurements were also taken into account?
	\end{enumerate}

%%%%
\subsubsection{Moment on gas deflector}
\label{exo_moment_gas_deflector}
\wherefrom{non-examniable}

	We revisit the exhaust gas deflector of exercise \ref{exo_exhaust_gas_deflector} p.~\pageref{exo_exhaust_gas_deflector}. \Cref{fig_deflector_sideview} below shows the deflector viewed from the side. The midpoint of the inlet is \SI{2}{\metre} above and \SI{5}{\metre} behind the wheel labeled~“\textbf{A}”, while the center axis of the outlet passes \SI{1,72}{\metre} away from it, as represented in fig~\ref{fig_deflector_sideview}
	\begin{figure}[ht!]
		\begin{center}
			\includegraphics[width=\textwidth]{deflector_sideview}
		\end{center}
		\supercaption{Side view of the mobile exhaust gas deflector which was shown in \cref{fig_deflector} p.~\pageref{fig_deflector}}{\wcfile{Exhaust gas deflector 2.svg}{Figure} \cczero \oc}
		\label{fig_deflector_sideview}
	\end{figure}

	What is the moment generated by the gas flow about the axis of the wheel labeled “\textbf{A}”?

%%%%
\subsubsection{Helicopter tail moment}
\label{exo_helicopter_tail_moment}
\wherefrom{non-examinable}

	In a helicopter, the role of the tail is to counter exactly the moment exerted by the main rotor about the main rotor axis. This is usually done using a tail rotor which is rotating around a horizontal axis.

	A helicopter, shown in figure~\ref{fig_notar}, is designed to use a tail \emph{without} a rotor, so as to reduce risks of accidents when landing and taking off. The tail is a long hollow cylindrical tube with two inlets and one outlet. To simplify calculations, we consider that pressure is atmospheric at every inlet and outlet.
	\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=0.9\textwidth]{notar}
		\end{center}
		\supercaption{Top-view of a helicopter using a rotor-less tail.}{\wcfile{Helicopter NOTAR tail moment calculation schematic.svg}{Figure} derived from \wcfile{MD 500C orthographical image.svg}{a figure} \ccbysa by \wcu{FOX 52}}
		\label{fig_notar}
	\end{figure}
	\begin{itemize}
		\item inlet A has a cross-section area of \SI{0,2}{\metre\squared}. It contributes hot exhaust gases of density \SI{0,8}{\kilogram\per\metre\cubed} and velocity \SI{12}{\metre\per\second}, aligned with the ($x$) axis of the tail;
		\item inlet B contributes \SI{25}{\kilogram\per\second} of atmospheric air incoming at an angle $\alpha = \SI{130}{\degree}$ relative to the axis of the tail, with a velocity of~\SI{3}{\metre\per\second}.
	\end{itemize}
	
	The mix of exhaust gases and atmospheric air is rejected at the tip of the tail (outlet C) with a fixed velocity of \SI{45}{\metre\per\second}. The angle $\theta$ at which gases are rejected is controlled by the flight computer.
	
	\begin{enumerate}
		\item What is the rejection angle $\theta$ required  so that the tail generates a moment of~\SI{+6}{\kilo\newton\metre} around the main rotor ($y$) axis?
		\item Propose and quantify a modification to the tail geometry or operating conditions that would allow the tail to produce no thrust (that is to say, zero force in the $x$-axis), while still generating the same moment.
	\end{enumerate}

	\textit{Remark: this system is commercialized by MD Helicopters as the \wed{NOTAR}{\textsc{notar}}. The use of exhaust gases was abandoned, however, a clever use of air circulation around the tail pipe axis contributes to the generated moment; this effect is explored in \chaptereleven (\S\ref{ch_circulating_cylinder} p.~\pageref{ch_circulating_cylinder}).}




%%%%
\begin{comment}
\subsubsection{Thrust reverser}
\label{exo_thrust_reverser}
\wherefrom{non-examinable}

	A turbofan installed on a civilian aircraft is equipped with a thrust inverter system. When the inverters are deployed, the “cold” outer flow of the engine is deflected outside of the engine nacelle.
	
	We accept that the following characteristics of the engine, when it is running at full power, and measured from a reference point moving with the engine, are independent of the aircraft speed and engine configuration:\\
		Inlet diameter						\tab $D_1 = \SI{2,5}{\metre}$\\
		Inlet speed							\tab $V_1 = \SI{60}{\metre\per\second}$\\
		Inlet temperature 					\tab $T_1 = \SI{288}{\kelvin}$\\
		Cold flow outlet velocity 	 		\tab $V_3 = \SI{85}{\metre\per\second}$\\
		Cold flow temperature  				\tab $T_3 = \SI{300}{\kelvin}$\\
		Hot flow outlet diameter 			\tab $D_4 = \SI{0,5}{\metre}$\\
		Hot flow outlet velocity 		 	\tab $V_4 = \SI{200}{\metre\per\second}$\\
		Hot flow outlet temperature 		\tab $T_4 = \SI{400}{\kelvin}$\\
		Air pressure at all exits 			\tab $p_3 = p_4 = p_\atm = \SI{1}{\bar}$\\
		Bypass ratio 						\tab $\dot m_3 / \dot m_4 = \num{6}$\\
	\begin{figure}[ht!]
	\vspace{-0.8cm}
	\begin{center}
		\includegraphics[width=0.9\textwidth]{turbofan_reverse}
	\end{center}
		\supercaption{Conceptual layout sketch of a turbofan. The airflow is from left to right.}{\wcfile{Simplified turbofan engine cutaway.svg}{Figure} \ccby \oc}
	\end{figure}
	
	We first study the engine in normal (forward thrust) configuration.
	\begin{enumerate}
		\item When mounted on a stationary test bench, what is the thrust provided by the engine?
		\item In the approximation that the operating parameters remain unchanged, what is the thrust generated when the engine moves with a speed of~\SI{40}{\metre\per\second} ?
	\end{enumerate}
	
	We now deploy the thrust reversers. They deflect the cold flow with a~\SI{75}{\degree} angle relative to the aircraft longitudinal axis.	
	\begin{enumerate}
		\shift{2}
		\item What is the net thrust developed when the aircraft velocity is \SI{40}{\metre\per\second}?
		\item What is the net thrust developed when the aircraft is stationary?
	\end{enumerate}
\end{comment}
	

\clearpage	%%%%
\subsubsection*{Answers}
\NumTabs{1}
\begin{description}
	\item [\ref{exo_pipe_bend}]%
					\tab 1) $\dot m = \SI{1,0603}{\kilogram\per\second}$
					\tab 2) $\vec F_\net = \left(\num{-0,5681};\num{-1,2184}\right) \si{\newton}$
					\tab 4) The force is quadrupled.
	\item [\ref{exo_exhaust_gas_deflector}]%
					\tab 1) $\vec F_\net = \left(\num{-9,532}; \num{+1,479}\right) \si{\kilo\newton}$ : $||\vec F_\net|| = \SI{9,646}{\kilo\newton}$ (force on ground is opposite: $\vec F_\text{fluid on pipe} = -\vec F_\text{pipe on fluid}$);
					\tab 3) $||\vec F_\text{net 2}|| = \SI{8,525}{\kilo\newton}$.
	\item [\ref{exo_pelton_turbine}]%
					\tab 1) $F_\net = \left(\num{-883,6}; 0\right) \si{\newton}$;
					\tab 2) $M_{\net\, X} = |F_\net| R = \SI{883,6}{\newton\metre}$;
					\tab 3) $\dot W_\text{rotor} = \SI{0}{\watt}$;
					\tab 4) $\omega = \SI{143,2}{rpm}$ ($F_\net = \SI{0}{\newton}$);
					\tab 5) $\dot W_\text{rotor} = \SI{0}{\watt}$ again;
					\tab 6) $\dot W_\text{rotor,\ max} = \SI{1,963}{\kilo\watt}$ @ $V_\text{blade, optimal} = \frac{1}{3} V_\text{water jet}$.
		\item [\ref{exo_snow_plow}]%
					\tab 1) $\dot m = \SI{2500}{\kilogram\per\second}$; $F_{\net\ x} = \SI{+10,07}{\kilo\newton}$, $F_{\net\ z} = \SI{-12,63}{\kilo\newton}$ (force on blade is opposite);
					\tab 2) $\dot W = \vec F_\net \cdot \vec V_\text{plow} = F_{\net x} |V_1| = \SI{69,94}{\kilo\watt}$
					\tab 3) $\dot W_2 = \num{1.1}^3 \dot W$ (\SI{+33}{\percent})
		\item [\ref{exo_pressure_losses_pipe_flow}]%
					\tab $V_\text{center} = \num{1,2245} U$%; $F_{\text{net} x} = \SI{+393}{\newton}$ (positive!)
	\item [\ref{exo_drag_cylindrical_profile}]%
					\tab $F_{\net x} = \rho L \int_{S2} \left(u_2^2 - U u_2\right) \diff y = \SI{-95,78}{\newton}$.
	\item [\ref{exo_drag_flat_plate}]%
					\tab $F_{\net x} = \rho L \int_0^\delta \left(u_{(y)}^2 - U u_{(y)}\right) \diff y = \SI{-7,175e-2}{\newton}$ : $\dot W_\text{drag} = U |F_{\net x}| = \SI{0,718}{\watt}$.
	\item [\ref{exo_drag_wing_profile}]%
					\tab $F_{\net x} \approx \rho L \Sigma_y \left[\left(u_2^2 - U u_2\right) \diffi y\right] = \SI{-64,8}{\newton}$.
	\item [\ref{exo_moment_gas_deflector}]%
					\tab Re-use $\dot m = \SI{67,76}{\kilogram\per\second}$, $V_1 = \SI{166,7}{\metre\per\second}$, $V_2 = \SI{33,94}{\metre\per\second}$ from ex.~\ref{exo_exhaust_gas_deflector}. With $R_{2 \perp V_2} = \SI{1,717}{\metre}$, plug in numbers in eq.~\ref{eq_rtt_angularmom_simple} p.~\pageref{eq_rtt_angularmom_simple}: $M_\net = \SI{+18,64}{\kilo\newton\metre}$ in $z$-direction.
	\item [\ref{exo_helicopter_tail_moment}]%
					\tab 1) Work eq.~\ref{eq_rtt_angularmom} down to scalar equation (in $y$-direction), solve for $\theta$: $\theta = \SI{123,1}{\degree}$. 
					\tab 2) There are multiple solutions which allow both moment and force equations to be solved at the same time. $r_\C$ can be shortened, the flow in C can be split into forward and rearward components, or tilted downwards etc. Reductions in $\dot m_\B$ or $V_\C$ are also possible, but quantifying them requires solving both equations at once.
\begin{comment}
	\item [\ref{exo_thrust_reverser}]%
					\tab $\dot m_\text{cold} = \SI{297}{\kilogram\per\second}$, $\dot m_\text{hot} = \SI{59,4}{\kilogram\per\second}$;
					\tab $F_\text{cold flow, normal, bench \& runway} = \SI{+74,25}{\kilo\newton}$,\\ $F_\text{hot flow, normal \& reverse, bench \& runway} = \SI{+8,316}{\kilo\newton}$;
					\tab $F_\text{cold flow, reverse, bench \& runway} = \SI{-24,35}{\kilo\newton}$ and $F_\text{hot flow, normal, bench \& runway} = \SI{+8,316}{\kilo\newton}$.
					\tab Adding the net pressure force due to the (lossless) flow acceleration upstream of the inlet, we obtain, on the bench: $F_\text{engine bench, normal} = \SI{-93,26}{\kilo\newton}$, $F_\text{engine bench, reverse} = \SI{+5,344}{\kilo\newton}$;
					and on the runway: $F_\text{engine runway, normal} = \SI{-92,5}{\kilo\newton}$ and $F_\text{engine bench, reverse} = \SI{+6,094}{\kilo\newton}$.
\end{comment}
\end{description}

\atendofexercises
