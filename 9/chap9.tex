\renewcommand{\lastedityear}{2019}
 \renewcommand{\lasteditmonth}{08}
   \renewcommand{\lasteditday}{13}
\renewcommand{\numberofthischapter}{9}
\renewcommand{\titleofthischapter}{\namechapternine}

\fluidmechchaptertitle
\label{chap_nine}


%%%%%%%%%%%
\section{Motivation}

	Most flows of interest to engineers and scientists are turbulent. Fluid flow in industrial and domestic piping, in engines and in turbomachinery, is turbulent. Flow close to solid surfaces and in the wake of objects is turbulent at all but the slowest speeds. Blood flow in our largest veins and arteries, and air flow in our nostrils and tracheae, are turbulent. River flows, ocean currents, and all but the calmest winds are turbulent.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=\textwidth]{Argentina.TMOA2003041_lrg.jpg}
			\end{center}
			\supercaption{Plankton blooming in the Atlantic ocean reveals the complexity of the flow passing over the coast of Argentina. The scale of the image is so that the height covers approximately \SI{500}{\kilo\metre} in this take.}{\attlink{https://frama.link/southatlanticphytoplankton}{Image} by Jacques Descloitres, MODIS Rapid Response Team NASA GSFC (\pd)}
			\label{fig_plankton_argentina}
		\end{figure}
	
	Turbulence may be ubiquitous, but it remains an incredibly complex phenomenon, and describing it accurately requires either extraordinarily powerful numerical computations, or advanced mathematics. Neither of those is available in this course.
	
	We are going to treat turbulence \emph{not} as a topic of research, but instead merely as an occurrence that, as engineers, we need to account for. We will try to answer the following two questions: In a flow, how to quantify and measure the degree of turbulence? And how to predict its degree and its rate of decay?
	
	In answering those, we will keep in mind that at our level, turbulence characterization is not an exact science: we expect that our models may be off by a factor of 10 or so. An imprecise or inexact understanding is better than none at all, and, at the very least, we are developing some familiarity with turbulence that will be useful in further studies, especially when exploring computational fluid dynamics or experimental fluid dynamics.
	

%%%%%%%%%%%
\section{Recognizing turbulence}
\coveredin{Tennekes \& Lumley \cite{tennekesetal1972}}

	%%%
	\subsection{A brief definition}
	
		We define \vocab{turbulence} as fluid motion in multiple scales which is chaotic and dissipative.
	
	
	%%%
	\subsection{Chaos, not randomness}
	\coveredin{Leschziner \cite{leschziner2015}}
		
		Turbulence is made possible by kinematic instability in the flow; this means that small disturbances, such as non-uniformities in the velocity distribution, are not significantly damped. The main dampening factor in fluids is viscosity~$\mu$, and so the main parameter which determines how stable a given flow is the Reynolds number (eq.~\ref{eq_def_reynolds_number}):
		\begin{IEEEeqnarray}{rCl}
			\re &\equiv& \frac{\rho V L}{\mu}
		\end{IEEEeqnarray}
		Typically, when $\re$ exceeds \num{1000}, the flow is very likely to be or become turbulent (figure~\ref{fig_rising_smoke}). We have seen in \chaptereight that this is because the magnitude of the Laplacian of the velocity field is \num{1000} times smaller than the magnitude of its advective. (Dampening factors other than viscosity sometimes also exist, such as density gradients or interaction with soft solid surfaces: in those cases, other non-dimensional parameters are used).
		
		\vimeothumb{84518319}{simulation of two miscible fluids of different densities layered one on top of the other (color representing density). The “perfect” uniform initial situation is unstable and leads to chaotic (hard to predict) patterns whose details will depend strongly on minute changes in the initial conditions. (A \textsc{2d} \dns simulation performed with MicroHH)}{Chiel van Heerwaarden (\ccby)}
		This instability is what gives turbulence its chaotic characteristic. If a rider-less bicycle is rolled forward and left to itself, it will continue rolling and eventually fall to the side. Which side, left or right, depends on the initial conditions: even a minute modification to the start position is likely to influence the result. The fall is deterministic, and can be calculated very precisely, but with a very strong dependence on the initial conditions.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.5\textwidth]{Smoke_Series_(4130758692).jpg}
			\end{center}
			\supercaption{A column of hot air from burning incense rising through cold air is an unstable situation. After a certain length, the flow breaks down into chaotic patterns. The occurrence is predicted across all fluids, plume diameters, and velocities: every time, the Reynolds number is the determining parameter.}{\wcfile{Smoke_Series_(4130758692).jpg}{Photo} \ccby by \attlink{https://500px.com/rafaespada}{Rafa Espada}}
			\label{fig_rising_smoke}
		\end{figure}

		Turbulent motion in fluids has the same properties. Fluid motion follows laws which are fully deterministic (the Navier-Stokes equation, eq.~\ref{eq_navierstokes}), but the exact patterns in situations where the Reynolds number is high cannot be predicted because, much like the for the bicycle above, they depend very minutely on the initial configuration.
		
		Thus, in two identical turbulent flow experiments, the details of the flow will be different. In this sense, turbulent flow is \vocab{chaotic} (depending extremely sensitively on initial conditions) but not \vocab{random}: it remains predictable, governed by well-known deterministic laws in which chance does not play a role. The effective engineer will determine 1) what general characteristics of turbulence \emph{do} remain identical in both flows, and 2) how they affect the main, global flow characteristics.
		
		
	%%%
	\subsection{Growth and decay}
	
		Turbulence occurs because of shear (sometimes also pressure) applied non-uniformly in a fluid. Typical sources are a sudden turn at the downstream side of an obstacle, and shear alongside solid walls. 
		
		Turbulence is only sustained if shear is continually applied to the flow (for example by the walls of a long pipe). If no sustaining source is provided, turbulence decays and eventually dies. This is a critical property: turbulence is \vocab{dissipative}. Large-scale motion created by an initial obstacle continually breaks down into ever smaller-scale motion. Ultimately, the motion becomes so small and slow that the energy is dissipated by viscosity into molecular-scale motion. 
		
		In this sense, turbulence is not magic: it occurs in circumstances that are well understood and documented. Turbulence takes energy out of the main flow (this is usually measurable as a streamwise pressure drop) and ultimately transforms it into heat.
	
	
	%%%
	\subsection{A cascade of vortices}
	\coveredin{Davidson \cite{davidson2015}}
	
		It is useful to think of turbulence in terms of \vocab{vortices} (also named \vocab{eddies}): parcels of rotating fluid. In a first approach, we can consider a turbulent fluid flow as made of two components: one main, mostly steady component (the time-average), and an additional secondary component, made of a complex chaotic tangle of vortices of multiple sizes.
		
		Turbulence begins when large vortices are created, whose size is approximately that of the largest obstacle in the flow path. When those vortices are stretched, they speed up (by virtue of conservation of angular momentum). When they are compressed, they buckle and twist, deforming into complex shapes and interacting with other vortices; this leads to their breakup into smaller vortices.
		
		A turbulent flow thus consists of a \vocab{cascade} of structures of decreasing size. Energy is always passed down from larger into smaller structures. Smaller vortices have lower diameter and feature lower velocities: they are then more strongly affected by viscosity. Ultimately, the smallest vortices disappear, their energy dissipated down into heat.
	
	
	%%%
	\subsection{Not turbulence}
	
		Not all unsteady flows are turbulent. Well-known patterns such as a \wed{von Karman vortex street}{von Kármán vortex street} (figure~\ref{fig_not_turbulence}) or a series of \wed{Wave cloud}{wave clouds}, for example, are not turbulent. Those oscillations occur at a single recognizable frequency and scale, and once the phenomena has begun, their evolution is easily predictable.		
		
		Most surface waves on a body of water (\eg waves in open sea, figure~\ref{fig_not_turbulence}) are not turbulence: although they may be partly chaotic, they dissipate very little energy (unless they crash on a shore) and propagate over very large distances. 
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.9\textwidth]{Clouds_over_the_Atlantic_Ocean_cropped} \vspace{0.05\textwidth}
				\includegraphics[width=0.9\textwidth]{Karmansche_Wirbelstr_kleine_Re}
			\end{center}
			\supercaption{Not turbulence: surface waves on the ocean (top) are complex, but not fully-chaotic, and they feature very little dissipation. Well-known oscillatory patterns such as the \wed{von Karman vortex street}{von Kármán vortex street} feature one dominant frequency and one dominant vortex size: they are not turbulent either.}{\wcfile{Clouds_over_the_Atlantic_Ocean.jpg}{Sea wave photo} \ccbysa by \wcun{Tfioreze}{Tiago Fioreze} (cropped)\\{\wcfile{Karmansche_Wirbelstr_kleine_Re.JPG}{Wake photo} \ccbysa by Jürgen Wagner}}
			\label{fig_not_turbulence}
		\end{figure}
		
		Molecular motion is, at the macroscopic scale, completely random, and will appear in measurements as Gaussian white noise with no distinguishable range of frequencies, and no dissipation phenomenon: it is not turbulence, either.
		
		Although more formal definitions of turbulence exist (see \eg \cite{pope2000,mathieuscott2000,davidson2015}), we are satisfied with focusing on just three requisites: a flow is turbulent if the motion contains many scales, is chaotic, and is dissipative.
		

%%%%%%%%%%%
\section{The effects of turbulence}
\coveredin{Tennekes \& Lumley \cite{tennekesetal1972}}


	%%%
	\subsection{Dissipation (losses)}
		
		Turbulence extracts energy from the main flow and dissipates it at a molecular level. This energy is never recovered. Turbulence therefore causes additional losses in the flow, that would not be present if it were to remain laminar. Typically, friction losses are increased by a factor 2 to 10.
		
		In bulk, turbulence has an effect similar to an increase in viscosity. A word of warning must follow this sentence: the degree of turbulence in a given flow is rarely uniform, and so this effect can rarely be quantified as a single value for the entire flow; instead, it must be computed locally.
		
		The increase in temperature caused by turbulent dissipation is not usually measurable, due to the high heat capacity of fluids. It usually does not significantly alter their density. Turbulence therefore acts as a mechanical energy dissipator for the flow.
		
		In some special cases, turbulence leads to a reduction in losses. We shall study one such occurrence in \chapterten, where we sometimes intentionally create \vocab{turbulent boundary layers}. This is not important at the moment.


	%%%
	\subsection{Main flow patterns}
	
		In any given flow, the laminar and turbulent regimes result in markedly different flow patterns. Turbulent flows always lead to wider shear zones and very unsteady patterns, which, when averaged over time, have more uniform velocity profiles.
		
		When the flow is clearly turbulent, increases in the Reynolds number do not lead to main flow pattern changes anymore. For example, jet plumes at $\red = \num{e5}$ and \num{e7} have nearly identical spread, length, and time-averaged velocity distribution. Some properties of turbulence still change with $\re$, as we will see below.
	
	
	%%%
	\subsection{Mixing}
	
		Turbulence tremendously increases mixing. The large range of the scales of motion (\ie the many different sizes of vortices which occur simultaneously) increases the contact surface between two mixing fluids, for example. This makes turbulence a desired property in many chemical reactors, or in cases where pollutants have to be dissipated (\eg for exhaust gases).

		The same features of turbulence greatly enhance heat transfer compared to laminar flow. Most heat exchangers for which space is important, such as radiators, feature turbulent flow.
		
		The increased mixing also affects exchange of momentum, with increase in the interaction between slower and faster fluid particles. This tends to widen areas of interaction between fluids of different velocities, such as plumes, exhaust stacks and shear layers.
		

%%%%%%%%%%%
\section{Quantifying turbulence}

	%%%
	\subsection{Average and fluctuation}
	
		For the purpose of quantifying turbulence, we distinguish, in a given flow, between the average velocity and the “turbulent part” of velocity, as illustrated in figure~\ref{fig_instantaneous_average}. We thus decompose the velocity field into two components: one is the \vocab{average} flow $\left(\overline u, \overline v, \overline w\right)$, and the other the \vocab{instantaneous fluctuation} flow $\left(u', v', w'\right)$:
		\begin{IEEEeqnarray}{rCl}
			u_i 			&\equiv& \overline u_i + u_i'\label{eq_def_average_u}\\
			\overline{u_i'} 	&\equiv& 0
		\end{IEEEeqnarray}
		
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.9\textwidth]{instantaneous_average}
			\end{center}
			\supercaption{An example of the separation between instantaneous and average values, here for temperature. The instantaneous temperature $T$ is decomposed as the sum of the time-averaged temperature $\overline T$ (blue curve) and the fluctuation $T'$, whose average $\overline{T'}$ is zero (red curve).}{\wcfile{Average and instantaneous values.svg}{Figure} \cczero \oc}
			\label{fig_instantaneous_average}
		\end{figure}


	%%%
	\subsection{Turbulence intensity}
	\coveredin{De Nevers \cite{denevers2004}}
	
		We define \vocab{turbulence intensity} $I$ as the average of the root-mean-square of the velocity fluctuations:
		\begin{IEEEeqnarray}{rCl}
			I &\equiv& \frac{1}{\overline V} \left[\frac{1}{3} \left[ \overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right]\right]^\frac{1}{2} \label{eq_i}
		\end{IEEEeqnarray}
		
		Turbulence intensity is a dimensionless local property representing the “strength” or “violence” of turbulence, usually expressed as a percentage. At each point in space, the root-mean-square of each of the three components of the velocity fluctuation is compared to the magnitude of the velocity.
		
		Typically, a low-turbulence channel flow would feature $I = \SI{3}{\percent}$, while a highly-turbulent channel flow could display for example $I = \SI{80}{\percent}$.
	
	
	%%%
	\subsection{The size of eddies}
	\coveredin{Leschziner \cite{leschziner2015}}
	
		When observing turbulence from the engineer’s point of view, it is useful to quantify two characteristic lengths: the size of the largest turbulent structures, and the size of the smallest structures. In that view, the cascade of turbulent eddies is contained between two limits: the turbulence-producing (large) scale, and the viscous dissipation (small) scale.
		
		\textbf{The largest structures} have a length whose order of magnitude is designated as $L_\max \equiv \Lambda$. It is observed that $\Lambda$ is typically \SI{50}{\percent} of the largest scale of the flow (somewhat more in unconfined flows, and somewhat less in very confined flows). For example, wind flow around a building will feature eddies whose maximum size is half the size of the building. Those eddies also feature a characteristic velocity $u_\Lambda$ (typically, the maximum measurable velocity in the eddy) and a characteristic time scale $t_\Lambda$ (typically, a duration sufficient to describe their movement fully), linked by a straightforward relation:
		\begin{IEEEeqnarray}{rCcCl}
			v_\Lambda = \frac{\Lambda}{t_\Lambda}
		\end{IEEEeqnarray}
		It follows that one may quantify a \vocab{large-scale eddy Reynolds number} based on those quantities:
		\begin{IEEEeqnarray}{rCcCl}
			\re_\Lambda &\equiv& \frac{\rho u_\Lambda \Lambda}{\mu}
		\end{IEEEeqnarray}
		
		
		\textbf{The smallest structures} have a length whose order of magnitude is designated as $L_\min \equiv \eta$. Likewise, they feature a characteristic velocity $u_\eta$ and a characteristic time scale $t_\eta$.\\		
		In the 1940s, \we{Andrey Kolmogorov} and his team developed a model to relate both the large and the small scales in the simplest occurrences of turbulence. In particular, Kolmogorov postulated that the characteristic Reynolds number of the smallest vortices (based on their characteristic length $\eta$ and speed $u_\eta$) is approximately equal to 1:
		\begin{IEEEeqnarray}{rCcCl}
			\re_\eta &\equiv& \frac{\rho u_\eta \eta}{\mu} &\approx& 1
		\end{IEEEeqnarray}
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=\textwidth]{PIA22256_North Atlantic_Mar1}
			\end{center}
			\supercaption{Surface-relative vorticity in the Atlantic ocean. Blue color indicates clockwise rotation, and red color anticlockwise rotation. Rotating structures of many different sizes can be observed. In homogeneous isentropic turbulence (and in this flow case by approximation), the size of the largest and smallest vortices are related to one another through the Reynolds number.}{\doilink{10.1038/s41467-018-02983-w} \ccby by Z.\ Su, J.\ Wang, P.\ Klein, A.\ F.\ Thompson \& D.\ Menemenlis \cite{suetal2018ocean}}
			\label{fig_vorticity_atlantic}
		\end{figure}
		
		Based on this postulate, when the turbulence has been given time and space enough to develop fully, is homogeneous, and isotropic (has identical properties in all three directions) —these are important restrictions—, Kolmogorov and his peers showed using dimensional analysis that
		\begin{mdframed}
		\begin{IEEEeqnarray}{rCcCl}
			\frac{L_\min}{L_\max} 	&=& \frac{\eta}{\Lambda} 	&=& \re_\Lambda^{-3/4}\label{eq_kolmogorov_size}\\
									&& \frac{u_\eta}{u_\Lambda} &=& \re_\Lambda^{-1/4}\label{eq_kolmogorov_speed}\\
									&& \frac{t_\eta}{t_\Lambda} &=& \re_\Lambda^{-1/2}\label{eq_kolmogorov_time}
		\end{IEEEeqnarray}
		\end{mdframed}
		These three equations are a very important result. They show that as the Reynolds number of a flow increases, the size and time scale of the smallest structures in the flow decreases. The higher the Reynolds number, the more complex and more minute the details of the flow become.		
		
		
	
	%%%
	\subsection{Turbulent kinetic energy and dissipation rate}
	\coveredin{De Nevers \cite{denevers2004}}
	
		We define \vocab{turbulent kinetic energy} $k$ as:
		\begin{IEEEeqnarray}{rCl}
			k &\equiv& \frac{1}{2} \left(\overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right)\label{eq_k}
		\end{IEEEeqnarray}
		
		Turbulent kinetic energy, measured in \si{\joule\per\kilogram}, represents the amount of energy per unit mass contained in the chaotic (turbulent) component of the fluid flow velocity.
		
		To convince oneself that the parameters are related, one may insert $I$  (eq.~\ref{eq_i}) into eq.~\ref{eq_k} to show that $k = 3/2 \ \overline{V}^2 I^2$.
		
		We define the \vocab{turbulent dissipation rate} $\epsilon$ as the rate at which turbulent kinetic energy is dissipating to heat. When no turbulence is produced, so that turbulence is simply left to decay, then $\epsilon$ is the time rate change of $k$:
		\begin{IEEEeqnarray}{rCl}
			\epsilon &=& -\partialtimederivative{k}
		\end{IEEEeqnarray}
		\begin{equationterms}	
			\item when no new turbulence is produced.
		\end{equationterms}
		The dissipation rate is measured in $\si{\watt\per\kilogram}$ and represents the local amount of turbulent kinetic energy that is currently being converted to heat through viscosity.
		
		Through dimensional analysis, Kolmogorov and his peers showed that in homogeneous, fully-developed and isotropic turbulence, the size, characteristic velocity, and characteristic time scale of the smallest eddies could be related to the dissipation rate with the relationships:
		\begin{mdframed}
		\begin{IEEEeqnarray}{rCl}
				\eta &=& \left(\frac{\mu^3}{\rho^3} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\label{eq_kolmogorov_epsilon_size}\\
				u_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\label{eq_kolmogorov_epsilon_vel}\\
				t_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{2}}\label{eq_kolmogorov_epsilon_time}
		\end{IEEEeqnarray}
		\end{mdframed}
		In other words, for completely-developed, homogeneous isotropic turbulence, if the characteristic size and speed of the largest turbulent structures are known, then the size of the smallest structures and the dissipation power can be known.
		
	
		In homogeneous, fully-developed, isotropic turbulence, it is interesting to observe how the turbulent kinetic energy and dissipation rate are distributed across the scales of the vortices. One can define the kinetic energy density $E$ as the kinetic energy of all the eddies in an eddy-size increment $\diff l$ in a volume of interest, divided by that increment:
		\begin{IEEEeqnarray*}{rCl}
			E &\equiv& \partialderivative{k}{l}
		\end{IEEEeqnarray*}
		In this way, the kinetic energy $k$ is recovered as the integral of $E$ with respect to $\diff l$:
		\begin{IEEEeqnarray}{rCl}
			k &=& \int_\eta^\Lambda E \diff l
		\end{IEEEeqnarray}
		In the same way, we may define the dissipation density $D$ as $\inlinepartialderivative{\epsilon}{l}$, so that the dissipation rate would be recovered as:
		\begin{IEEEeqnarray}{rCl}
			\epsilon &=& \int_\eta^\Lambda D \diff l
		\end{IEEEeqnarray}
		These two integral equations are only useful to understand the meaning of figure~\ref{fig_cascade_k_epsilon}, where the distribution of $k$ and $\epsilon$ across the scales of eddies is plotted.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.9\textwidth]{spectra}
			\end{center}
			\supercaption{Distribution of turbulent kinetic energy (left) and of turbulent dissipation rate (right) in fully-developed homogeneous isotropic turbulence. The top diagrams are in linear scale, while the bottom diagrams are in logarithmic scale. In those diagrams, the horizontal axis displays $1/l$, so that the small-scale eddies are on the right side, and large-scale eddies are on the left side. \\Those energy and dissipation distributions are for the simplest occurrences of turbulence; their features (in particular, the curves’ slopes and the ratios between $\Lambda$ and $\epsilon$) are used as reference cases in the study of more complex cases.}{\wcfile{Distribution turbulent kinetic energy and dissipation rate.svg}{Figure} \ccbysa by \olivier}
			\label{fig_cascade_k_epsilon}
		\end{figure}
		
		In this figure~\ref{fig_cascade_k_epsilon}, one may see that \emph{most of the energy is contained in the large eddies}, while \emph{most of the dissipation occurs in the small eddies}.
		
		
	
	%%%
	\subsection{Turbulence anisotropy and inhomogeneity}
	
		The relationships between largest and smallest scales derived by Kolmogorov only hold true for very “straightforward” turbulence. In most flows of interest, turbulence is being created and dissipated in more complex ways. In such cases, which are outside the scope of this course, important properties to quantify are:
		\begin{description}
			\item \vocab{Inhomogeneity} (uneven distribution in space). The properties quantified above are observed in different places, often revealing that production and dissipation of turbulence occur in different areas;
			\item \vocab{Anisotropy} (dependence on direction). The properties quantified above are observed in different directions. Anisotropy is most often quantified by evaluating correlations between velocity fluctuations, and the skewness (asymmetry) of their distributions.
			\item \vocab{Development}. Sometimes, not enough time has passed for the full spectrum of scales to appear, because the energy has not cascaded down to the smallest scales yet.
		\end{description}

		In most of those cases, the basic models studied above serve as reference cases, against which comparisons can be made.


%%%%%%%%%%%
\section{Computing turbulent flow}


	%%%
	\subsection{Basic premise}
	
		We have seen in \S\ref{ch_cfd} that in principle, the flow of fluids can be computed for any given flow by solving for the change in time of the unknowns $u$, $v$ and $w$ in the Navier-Stokes equation. Such a formulation is called a \vocab{Direct Numerical Simulation} (\textsc{dns}); it allows solving for \emph{all} flows and will very well describe turbulent flows.
		
		\youtubethumb{aR-hehP1pTk}{\dns simulation of air flow over an airfoil at relatively low speeds ($\re = \num{4e5}$, so $V \approx \SI{50}{\kilo\metre\per\hour}$). Because the complete details of the flow are solved, \num{35} million \textsc{cpu}-hours were needed for this calculation in 2015. On an ordinary desktop computer, this would take 500 years to complete.}{Linné FLOW Centre (\styl)}
		Unfortunately, the reality is that the computational cost of \textsc{dns} is enormous, and precludes us from solving most flows of interest. Turbulence exacerbates the problem. With eqs.~\ref{eq_kolmogorov_size} \& \ref{eq_kolmogorov_time} we can see that as the Reynolds number increases, the spatial and temporal discretization of the computation must increase, too. Every decrease in the size of the grid cell and in the length of the time step increases the total number of equations to be solved by the algorithm. Halving each of $\delta x$, $\delta y$,  $\delta z$ and $\delta t$ multiplies the total number of equations by \num{16}, so that soon enough the designer of the simulation will wish to know what maximum (coarsest) grid size is appropriate or tolerable. Furthermore, in many practical cases, we may not even be interested in an exhaustive description of the velocity field, and just wish to obtain a general, coarse description of the fluid flow.


	%%%
	\subsection{Accounting for turbulence}
	\coveredin{Versteeg et al.\ \cite{versteegetal2007}, Leschziner \cite{leschziner2015}, and Wilkes \cite{wilkes2006}}

		The decomposition of the flow into two components $\overline u$ and $u'$ which we performed in eq.~\ref{eq_def_average_u} earlier is useful when we wish to simulate the flow numerically (performing \vocab{computational fluid dynamics} or \textsc{cfd}). Let us now say that $\overline u$ is the component of velocity that is captured by the simulation, while $u'$ is the component which is too small, or occurring too quickly, for the simulation to capture. Inserting the definition~\ref{eq_def_average_u} into the $x$-component of the Navier-Stokes equation for incompressible flow, we obtain:
		\begin{multline}
				\rho \left[ \partialtimederivative{(\overline u + u')} + (\overline u + u') \partialderivative{(\overline u + u')}{x} + (\overline v + v') \partialderivative{(\overline u + u')}{y} + (\overline w + w') \partialderivative{(\overline u + u')}{z} \right] \nonumber\\
					= \rho g_x - \partialderivative{(\overline p + p')}{x} + \mu \left[ \secondpartialderivative{(\overline u + u')}{x} + \secondpartialderivative{(\overline u + u')}{y} + \secondpartialderivative{(\overline u + u')}{z} \right]
			\end{multline}
		
		Taking the \emph{average} of this equation —thus expressing the dynamics of the flow as we calculate them with a finite, coarse grid— yields, after some intimidating but easily conquerable algebra:
		\begin{IEEEeqnarray}{lr}
				\rho \left[ \partialtimederivative{\overline u} + \overline u \partialderivative{\overline u}{x} + \overline v \partialderivative{\overline u}{y} + \overline w \partialderivative{\overline u}{z}\right] + \rho \left[\overline{u' \partialderivative{u'}{x}} + \overline{v' \partialderivative{u'}{y}} + \overline{w' \partialderivative{u'}{z}} \right] &\nonumber\\
					~~~~~~ = \rho g_x - \partialderivative{\overline p}{x} + \mu \left[ \secondpartialderivative{\overline u}{x} + \secondpartialderivative{\overline u}{y} + \secondpartialderivative{\overline u}{z} \right] &\label{eq_rans}
		\end{IEEEeqnarray}
		
		Equation~\ref{eq_rans} is the $x$-component of the \vocab{Reynolds-averaged Navier-Stokes equation} (\textsc{rans}). It shows that when one observes the flow in terms of the sum of an average and an instantaneous component, the dynamics cannot be expressed solely according to the average component. Comparing eqs.~\ref{eq_rans} and~\ref{eq_ns_cartone} we find that an additional term has appeared on the left side. This term, called the \vocab{Reynolds stress}, is often re-written as $\rho \ \inlinepartialderivative{\overline{u_i' u_j'}}{j}$. In turbulent flow, it is \emph{not zero}, because the instantaneous fluctuations of velocity (\eg $u'$ and $v'$) are strongly correlated: they are each zero on average, but their multiples are not.
		
		The difference between eqs.~\ref{eq_rans} and~\ref{eq_ns_cartone} can perhaps be expressed differently: the time-average of a turbulent flow cannot be calculated by solving for the time-average velocities. Or, more bluntly: \emph{the average of the solution cannot be obtained with only the average of the flow}. This is a tremendous burden in computational fluid dynamics, where limits on the available computational power prevent us in practice from solving for these fluctuations. 
		
		In the overwhelming majority of computations, the Reynolds stress has to be approximated in bulk with schemes named \vocab{turbulence models}. That is, a local value for $\rho \ \inlinepartialderivative{\overline{u_i' u_j'}}{j}$ is estimated everywhere, depending on the average values ($\overline{u_i}$). The most well-known method for doing this is the \mbox{\textit{$k$-epsilon}} turbulence model, which involves solving partly arbitrary transport equations for both $k$ and $\epsilon$. The delights, shortcomings and mysteries of that method and more are left for the reader to discover in a good hands-on course on the youngest and most promising area of this discipline, \cfd.


%%%%%%%%%%%
\section{Commented bibliography}

	Most books on turbulence spill into mathematical intricacies which are irrelevant to the engineer; it is regrettable that some of the very best documentation on turbulence is in the user manuals of \cfd software and experimental measurement devices. It is also regrettable that few books provide actual applied \emph{problems} to be solved quantitatively. A commented (and necessarily subjective) bibliography for learning about turbulence is proposed here:
	\begin{itemize}
		\item General understanding (encyclopedic knowledge)\\
			The introductions of Mathieu \& Scott~\cite{mathieuscott2000}, of Tennekes \& Lumley~\cite{tennekesetal1972}, and of Davidson~\cite{davidson2015} will provide excellent information.
		\item Reference works\\
			Unfortunately, no book truly aimed at engineers is known to the author. The following books provide in-depth insight over the physics of turbulence:
			\begin{itemize}
				\item Tennekes \& Lumley~\cite{tennekesetal1972}: despite its age, an outstanding book, in particular for its first chapter.
				\item Leschziner~\cite{leschziner2015}: despite its focus on \cfd, the book serves as a great step-by-step exploration of turbulence.
				\item Davidson~\cite{davidson2015} (reference book). Exquisitely referenced.
				\item Mathieu \& Scott~\cite{mathieuscott2000} (reference book). An appropriate amount of comment is provided around mathematical expressions.
				\item Pope~\cite{pope2000} (reference book). Terser than its counterparts above.
			\end{itemize}
		\item Works containing useful problems\\
				De Nevers~\cite{denevers2004} (a single chapter within) is a very applied, down-to-earth and useful treatment; Tennekes \& Lumley~\cite{tennekesetal1972} also contains good examples in the first chapter.
		\item Dealing with turbulence in \cfd\\
				The best coverage is probably in Leschziner~\cite{leschziner2015}. Some useful (passing) information can be found in Tu et al.~\cite{tuetal2018computational} and Versteeg et al.~\cite{versteegetal2007}.
		\item Books with parts useful for specific purposes include:
			\begin{itemize}
				\item Bernard \& Wallace~\cite{bernard2002turbulent}, for theory about experiments;
				\item Libby~\cite{libby1996introduction}, for its introduction to applied statistics;
				\item Sagaut~\cite{sagaut2006les}, for its coverage of the notion of spectra, and its excellent illustrations;
				\item Cebeci~\cite{cebeci2004analysisturbulentflows}, for its rigorous introduction into basic metrics, and excellent fundamental data and diagrams.
			\end{itemize}
	\end{itemize}

\atendofchapternotes
