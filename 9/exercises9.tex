\renewcommand{\lastedityear}{2019}
 \renewcommand{\lasteditmonth}{08}
   \renewcommand{\lasteditday}{13}
\renewcommand{\numberofthischapter}{9}
\renewcommand{\titleofthischapter}{\namechapternine}
\atstartofexercises

\fluidmechexercisestitle

\mecafluboxen

\mecafluexboxen

\begin{boiboiboite}
Turbulence intensity $I$:
	\begin{IEEEeqnarray}{rCl}
		I &\equiv& \frac{1}{\overline V} \left[\frac{1}{3} \left[ \overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right]\right]^\frac{1}{2} \ztag{\ref{eq_i}}
	\end{IEEEeqnarray}

Turbulent kinetic energy $k$:
	\begin{IEEEeqnarray}{rCl}
		k &\equiv& \frac{1}{2} \left(\overline{\left(u'^2\right)} + \overline{\left(v'^2\right)} + \overline{\left(w'^2\right)} \right)\ztag{\ref{eq_k}}
	\end{IEEEeqnarray}

In homogeneous, isotropic, fully-developed turbulence, the following relationships apply between the largest-scale and smallest-scale eddies:
	\begin{IEEEeqnarray}{rCcCl}
		\frac{L_\min}{L_\max} 	&=& \frac{\eta}{\Lambda} 	&=& \re_\Lambda^{-3/4}\ztag{\ref{eq_kolmogorov_size}}\\
								&& \frac{u_\eta}{u_\Lambda} &=& \re_\Lambda^{-1/4}\ztag{\ref{eq_kolmogorov_speed}}\\
								&& \frac{t_\eta}{t_\Lambda} &=& \re_\Lambda^{-1/2}\ztag{\ref{eq_kolmogorov_time}}
	\end{IEEEeqnarray}
	\begin{IEEEeqnarray}{rCl}
				\eta &=& \left(\frac{\mu^3}{\rho^3} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\ztag{\ref{eq_kolmogorov_epsilon_size}}\\
				u_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{4}}\ztag{\ref{eq_kolmogorov_epsilon_vel}}\\
				t_\eta &=& \left(\frac{\mu}{\rho} \frac{1}{\epsilon} \right)^{\frac{1}{2}}\ztag{\ref{eq_kolmogorov_epsilon_time}}
	\end{IEEEeqnarray}

\end{boiboiboite}

\clearpage
\subsubsection{Hypothetical flow}
\label{exo_hypothetical_flow}
\wherefrom{From De Nevers \cite{denevers2004} Ex 18.1}

	We imagine a turbulent flow described at some point with the equations (in \si{\metre\per\second})
		\begin{IEEEeqnarray}{rCl}
			u &=& 10 + \sin t\\
			v &=& 0\\
			w &=& 0
		\end{IEEEeqnarray}
	(No real turbulent flow can be described by equations this simple — but this is a nice first basis for practice)
	
	What are the values of $\overline{u}$, $u'$, $\overline{u'}$, $I_x$, $I$, and $k$?
	
	Hint: $\int \sin^2 x \diff x = \frac{1}{2}\left(x + \frac{\sin 2x}{2} \right) + b$


\subsubsection{Turbulent channel flow}
\label{exo_turbulent_channel_flow}
\wherefrom{From De Nevers \cite{denevers2004} Ex 18.2}

	A wind tunnel carries air through a channel which is \SI{1}{\metre} wide and \SI{0.24}{\metre} high. The average velocity is \SI{0.82}{\metre\per\second}. The pressure drop caused by both friction on the walls and turbulent dissipation is measured at \SI{-0,0286}{\pascal\per\metre}.
	\begin{enumerate}
		\item What is the non-turbulent kinetic energy per unit mass of the flow? 
		\item At what average rate does this kinetic energy degrade into heat?
		\item If there was no heat transfer, what would be the rate of temperature increase of the air?
	\end{enumerate}
	
	Measurements are carried out to measure the turbulent intensity through the channel. Those are displayed in figure~\ref{fig_t_measurements}. 
		\begin{figure}[ht]
			\begin{center}
				\vspace{-0.5cm}
				\includegraphics[width=8cm]{t_measurements}
				\vspace{-0.5cm}
			\end{center}
			\supercaption{Measurements of turbulent intensity in $x$ and $y$ directions in a rectangular channel \SI{1}{\metre} wide and \SI{0.24}{\metre} wide, in which the centerline velocity $\overline v$ is \SI{1}{\metre\per\second}. Here $u'$ and $v'$ are written $v_x$ and $v_y$ respectively.}{Figure extracted from De Nevers \cite{denevers2004}, with source data from Reichardt 1938, \textit{Naturwissenschaften 26}:407}
			\label{fig_t_measurements}\vspace{-0.5cm}
		\end{figure}
	\begin{enumerate}
		\shift{3}
		\item What is the value of $k$ at a point \SI{2}{\centi\metre} from the wall?
	\end{enumerate}

\begin{comment}
	A computational fluid dynamics (\cfd) simulation of the flow is carried out, in which $C_\mu = \num{0,09}$. It predicts that the value of $\epsilon$ at the point where $k$ was calculated above is $\epsilon = \SI{0,025}{\metre\squared\per\second\cubed}$.
	
	\begin{enumerate}
		\shift{3}
		\item What is the value of the turbulent viscosity at the point where $k$ was measured above, and how does it compare to the value of the viscosity of the air?
	\end{enumerate}
\end{comment}

\subsubsection{Cumulus cloud}
\label{exo_cumulus_cloud}
\wherefrom{From Tennekes \& Lumley \cite{tennekesetal1972} P1.1}

	A cumulus cloud (one of those “fluffy” summer clouds, figure~\ref{fig_fluffy_cloud}) has roughly the size of a sphere of diameter $D = \SI{50}{\meter}$. To a good approximation, it features isotropic, homogeneous, fully-developed turbulence. The largest-scale air currents in the cloud reach a maximum velocity $V = \SI{3}{\metre\per\second}$.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.3\textwidth]{Bluesky.jpg}
			\end{center}
			\supercaption{Summer clouds (\textit{Cumulus humilis}) form when hot moist air convected from the ground is cooled down when it rises.}{\wcfile{Bluesky.jpg}{Photo} \ccbysa by \weu{Dwindrim}}
			\label{fig_fluffy_cloud}
		\end{figure}
	\begin{enumerate}
		\item What is approximately the size of the smallest eddies in the cloud?
		\item What is approximately the dissipation power, per unit mass of air and for the entire cloud?
		\item What will those three values become once the cloud has grown to a diameter of $D_2 = \SI{100}{\meter}$?
	\end{enumerate}

\subsubsection{Reactor tank}
\label{exo_reactor_tank}
%\wherefrom{\cczero \oc}

	A tank used to store chemical reactants has roughly the size of a cube of side length $L = \SI{2}{\metre}$ (figure~\ref{fig_tank_agitator}). The tank is filled with a water-like liquid and vigorously stirred with a large agitator propeller for a prolonged amount of time. The propeller induces a maximum fluid velocity of \SI{1,5}{\metre\per\second}.
		\begin{figure}[ht]
			\begin{center}
				\includegraphics[width=0.3\textwidth]{tank_agitator}
			\end{center}
			\supercaption{Schematic drawing of a cubic tank stirred with an agitator propeller}{Figure \cczero \oc}
			\label{fig_tank_agitator}
		\end{figure}
	\begin{enumerate}
		\item What is approximately the size of the smallest eddies in the tank?
		\item What is approximately the specific dissipation power?
	\end{enumerate}

	A full-scale simulation (\textsc{dns}) of the flow was carried out, which required 500 hours of computing time on a supercomputer. Now the same simulation is to be carried out again, for the same flow, but with a fluid whose viscosity is half that of water.
	\begin{enumerate}
		\shift{2}
		\item What do you expect the new computation time to be?
	\end{enumerate}
	

\clearpage	
\subsubsection*{Answers}
\startofanswers
\begin{enumerate}
	\item p.~\pageref{exo_hypothetical_flow}
		\begin{enumerate}
			\item $\overline u = \SI{10}{\metre\per\second}$
			\item $u' = \sin (t)$
			\item $\overline u' = \SI{0}{\metre\per\second}$ (as always)
			\item $I_x = \SI{7,07}{\percent}$
			\item $I = \SI{4,08}{\percent}$
			\item $k = \SI{0,25}{\joule\per\kilogram}$
		\end{enumerate}
	\item p.~\pageref{exo_turbulent_channel_flow}
		\begin{enumerate}
			\item $\dot e_{m \text{main}} = \SI{0,336}{\joule\per\kilogram}$
			\item $\epsilon = \SI{0,0191}{\watt\per\kilogram}$
			\item $\dot T = \SI{0,02}{\milli\kelvin\per\second}$
			\item $k = \SI{5,76}{\milli\joule\per\kilogram}$
		\end{enumerate}
	\item p.~\pageref{exo_cumulus_cloud}
		\begin{enumerate}
			\item Since $\re_\Lambda \equiv \num{6,1e3}$, $\eta \approx \SI{0,2}{\milli\metre}$
			\item $\epsilon \approx \SI{1,147}{\watt\per\kilogram}$ \& $\dot W_\epsilon \approx \SI{92}{\kilo\watt}$
			\item $\eta_2 = \num{0,6} \eta_1$, $\epsilon_2 = 8 \epsilon_1$, $\dot W_{\epsilon 2} = \num{64} \dot W_{\epsilon 1}$
		\end{enumerate}
	\item p.~\pageref{exo_reactor_tank}
		\begin{enumerate}
			\item $\eta \approx \SI{0,023}{\milli\metre}$ (width of human hair)
			\item $\epsilon \approx \SI{3,4}{\watt\per\kilogram}$
			\item Increase resolution in all three directions according to $\eta$, and the time resolution according to $t_\eta$: the computation time increases by a factor \num{6,7}.
		\end{enumerate}
\end{enumerate}

\atendofexercises
